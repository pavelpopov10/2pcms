<?
/***************************************************
*	2PCMS by Pavel Popov © 2013
*	Расположение: ./index.php
*	Описание: Точка входа в приложение
***************************************************/

/* Вывод ошибок */
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 'On');

/* Системные константы */
define('DS', 				DIRECTORY_SEPARATOR);
define('SYSROOT',			realpath(dirname(__FILE__)).DS);
	define('COREPATH',			realpath(SYSROOT.'core').DS);
	define('CLASSPATH',			realpath(COREPATH.'classes').DS);
	define('MODPATH',			realpath(SYSROOT.'modules').DS);
	define('INCPATH',			realpath(SYSROOT.'includes').DS);
	define('MEDIAPATH',			realpath(SYSROOT.'media').DS);
	define('LOGPATH',			realpath(SYSROOT.'logs').DS);
	define('TPLPATH',			realpath(SYSROOT.'tpl').DS);
	define('I18NPATH',			realpath(SYSROOT.'i18n').DS);
/* Проверка на наличие установщика */
if (file_exists('install.php')) {$_POST['install_mode'] = md5('goodidea');}
elseif (!file_exists(COREPATH.'dbconf.ini') || !file_exists(TPLPATH.'routes.ini')) {die('ERROR [INIT_NI]: System not installed and installer was not found!');}
/* Запускаем загрузчик ядра */
if (file_exists(COREPATH.'bootstrap.php')){include COREPATH.'bootstrap.php';}
else {die('ERROR [INIT_0]: No core bootstrap found!');}
/* Загружаем файл установки */
if (isset($_POST['install_mode'])) {include 'install.php';}
?>