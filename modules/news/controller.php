<?
class Module_news_controller extends _MODULE
{
	private static $dependency;
	private static $model;
	private static $news;
	private static $auth_obj;
	private static $acts_checked = FALSE;
	private static $cares;
	private static $curr_post;
	private static $post_id;

	public function info() {return array('name' => 'Standart News', 'version' => '0.0.4', 'author' => 'PavelPopov', 'compatible' => '0.1.7_Alpha_20140827_bferrtlf', 
	/* @todo: i18n компонентов */
	'components' => array(
		array('name' => 'GetNews', 'desc' => 'Use it to get news feed', 'return' => 'news feed', 'link' => 'news'),
		array('name' => 'ActionCompleted', 'desc' => 'Use it to check if action was completed', 'return' => 'TRUE if action was completed and FALSE otherwise', 'link' => 'checkactcomp'),
		array('name' => 'GetPostTitle', 'desc' => 'Use it to get current post title', 'return' => 'post title', 'link' => 'ptitle'),
		array('name' => 'GetPostShort', 'desc' => 'Use it to get current post short', 'return' => 'post short', 'link' => 'pshort'),
		array('name' => 'GetPostFull', 'desc' => 'Use it to get current post full', 'return' => 'post full', 'link' => 'pfull'),
		array('name' => 'IsIdSet', 'desc' => 'Use it to check if post id isset', 'return' => 'TRUE if post id isset and FALSE otherwise', 'link' => 'checkidset'),
		array('name' => 'GetId', 'desc' => 'Use it to get post id', 'return' => 'post id', 'link' => 'getid'),
	))+parent::info();}
	
	
	function __construct()
	{
		if (isset($_POST['id'])) {self::$post_id = intval($_POST['id']);} elseif (isset($_GET['id'])) {self::$post_id = intval($_GET['id']);}
		if (!_REQUEST::isJustForInfo())
		{
			if (self::$model == null)
			{
				if ($this->inc('model.php')) {self::$model = new Module_news_model();} else {trigger_error($this->getCorruptError(), E_USER_ERROR);}
				if (self::$dependency == null) {self::$dependency = _MODMAN::isModuleInstalled('Standart CP');}
				if (self::$dependency === FALSE) {trigger_error($this->getDependencyError(), E_USER_ERROR);}
				if (!self::$acts_checked) {self::$cares = $this->checkacts(); self::$acts_checked = TRUE;}
			}
		}
	}
	
	public function checkidset()
	{
		return (self::$post_id !== null);
	}
	public function getid()
	{
		return self::$post_id;
	}
	
	/* @todo: Убрать дублирование кода путём вызова методов с параметрами */
	public function ptitle()
	{
		if (self::$post_id !== null)
		{
			if (self::$curr_post === null) {self::$curr_post = self::$model->getPostById(self::$post_id);}
			return self::$curr_post['title'];
		}
	}
	public function pshort()
	{
		if (self::$post_id !== null)
		{
			if (self::$curr_post === null) {self::$curr_post = self::$model->getPostById(self::$post_id);}
			return self::$curr_post['short'];
		}
	}
	public function pfull()
	{
		if (self::$post_id !== null)
		{
			if (self::$curr_post === null) {self::$curr_post = self::$model->getPostById(self::$post_id);}
			return self::$curr_post['full'];
		}
	}
	
	public function checkactcomp()
	{
		return (self::$cares !== null);
	}
	
	public function news()
	{
		if (self::$news == null) {self::$news = self::$model->getAllNews();}
		$content = '';
		foreach (self::$news as $post)
		{
			$user = self::$model->getUserById($post['author']);
			if (!empty($user))
			{
				$post['author'] = $user['login'];
			} else {
				$post['author'] = 'N/A';
			}
			$content .= $this->view('post', $post);
		}
		return $content;
	}
	
	private function checkacts()
	{
		$f = FALSE;
		$act = '';
		$post_title = null;
		$post_short = null;
		$post_full = null;
		$post_author = null;
		if (isset($_POST['act'])) {$act = $_POST['act'];} elseif (isset($_GET['act'])) {$act = $_GET['act'];}
		if (isset($_POST['title'])) {$post_title = $_POST['title'];} elseif (isset($_GET['title'])) {$post_title = $_GET['title'];}
		if (isset($_POST['short'])) {$post_short = $_POST['short'];} elseif (isset($_GET['short'])) {$post_short = $_GET['short'];}
		if (isset($_POST['full'])) {$post_full = $_POST['full'];} elseif (isset($_GET['full'])) {$post_full = $_GET['full'];}
		if (isset($_POST['author'])) {$post_author = intval($_POST['author']);} elseif (isset($_GET['author'])) {$post_author = intval($_GET['author']);}
		switch($act)
		{
			case 'addnewspost':
				if ($post_title === null || $post_short === null || $post_full === null) {$f = TRUE;}
				
				if (!$f)
				{
					/* Проверка на кол-во символов */
					$post_title_strlen = strlen($post_title);
					$post_short_strlen = strlen($post_short);
					$post_full_strlen = strlen($post_full);
					if ($post_title_strlen < 5 || $post_short_strlen < 10 || $post_full_strlen < 10
					|| $post_title_strlen > 256 || $post_short_strlen > 4096 || $post_full_strlen > 10240) {$f = TRUE;}
				}
				if (!$f)
				{
					/* Получение id */
					if (($res =  self::$model->getMaxId()) === FALSE) {$f = TRUE;}
					else {
						if ($res === null)
						{
							self::$post_id = 0;
						} else {
							self::$post_id = $res+1;
						}
					}
				}
				if (!$f)
				{
					$post_title = Core::secure($post_title);
					$post_short = Core::secure($post_short);
					$post_full = Core::secure($post_full);
					if ($post_author === null) {$post_author = 0;}
				
					if ($f || self::$model->addPost(self::$post_id, $post_title, $post_short, $post_full, $post_author) === FALSE) {$f = TRUE;}
				}
				return ($f?null:1);
			case 'editnewspost':
				if (self::$post_id === null || $post_title === null || $post_short === null || $post_full === null) {$f = TRUE;}
				
				if (!$f)
				{
					/* Проверка на кол-во символов */
					$post_title_strlen = strlen($post_title);
					$post_short_strlen = strlen($post_short);
					$post_full_strlen = strlen($post_full);
					if ($post_title_strlen < 5 || $post_short_strlen < 10 || $post_full_strlen < 10
					|| $post_title_strlen > 256 || $post_short_strlen > 4096 || $post_full_strlen > 10240) {$f = TRUE;}
				}
				if (!$f)
				{
					if ($f || self::$model->updPost(self::$post_id, $post_title, $post_short, $post_full) === FALSE) {$f = TRUE;}
				}
				return ($f?null:2);
			case 'delnewspost':
				if (self::$post_id !== null)
				{
					if (self::$model->delPost(self::$post_id) === FALSE) {$f = TRUE;}
				} else {$f = TRUE;}
				return ($f?null:3);
			default: 
				return null;
		}
	}
}
?>