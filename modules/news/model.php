<?
class Module_news_model
{
	/* @todo: Сделать какую-то возможность использовать методы модулей извне. Вообщем как-то убрать дупликацию кода. */
	public function getUserById($id)
	{
		$lmi = _PDO::q(Core::$_db, "SELECT * FROM `users` WHERE `id` = :id", array('id' => $id));
		if ($lmi === FALSE) {return FALSE;}
		return $lmi->fetch(PDO::FETCH_ASSOC);
	}

	public function getNewsById($id)
	{
		$lmi = _PDO::q(Core::$_db, "SELECT * FROM `news` WHERE `id` = :id", array('id' => $id));
		if ($lmi === FALSE) {return FALSE;}
		return $lmi->fetch(PDO::FETCH_ASSOC);
	}
	
	public function getAllNews()
	{
		$lmi = _PDO::q(Core::$_db, "SELECT * FROM `news` ORDER BY `dt` DESC");
		if ($lmi === FALSE) {return FALSE;}
		return $lmi->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function getPostById($id)
	{
		$lmi = _PDO::q(Core::$_db, "SELECT * FROM `news` WHERE `id` = :id", array('id' => $id));
		if ($lmi === FALSE) {return FALSE;}
		return $lmi->fetch(PDO::FETCH_ASSOC);
	}
	
	public function getMaxId()
	{
		$lmi = _PDO::q(Core::$_db, "SELECT MAX(`id`) FROM `news`");
		if ($lmi === FALSE) {return FALSE;}
		return $lmi->fetch()[0];
	}
	
	public function addPost($id, $title, $short, $full, $author)
	{
		return _PDO::q (Core::$_db, "INSERT INTO `news` (`id`, `title`, `short`, `full`, `author`, `dt`) VALUES (:id, :title, :short, :full, :author, NOW())",
		array(	'id' => $id,
				'title' => $title,
				'short' => $short,
				'full' => $full,
				'author' => $author
		));
	}
	
	public function updPost($id, $title, $short, $full)
	{
		return _PDO::q (Core::$_db, "UPDATE `news` SET `title`=:title, `short`=:short, `full`=:full, `dt`=NOW() WHERE `id` = :id",
		array(	'id' => $id,
				'title' => $title,
				'short' => $short,
				'full' => $full
		));
	}
	
	public function delPost($id)
	{
		return _PDO::q (Core::$_db, "DELETE FROM `news` WHERE `id` = :id",
		array('id' => $id));
	}
}
?>