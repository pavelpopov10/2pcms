CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `short` varchar(4096) NOT NULL,
  `full` varchar(10240) NOT NULL,
  `author` int(11) NOT NULL,
  `dt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;