<?
class Module_admin_model
{
	public function getModulesByParent($pname)
	{
		$lmi = _PDO::q(Core::$_db, "SELECT * FROM `modules` WHERE `pname` = :pname", array('pname' => $pname));
		if ($lmi === FALSE) {return FALSE;}
		return $lmi->fetchAll();
	}
	
	public function getModules()
	{
		$lmi = _PDO::q(Core::$_db, "SELECT * FROM `modules`");
		if ($lmi === FALSE) {return FALSE;}
		return $lmi->fetchAll();
	}
}
?>