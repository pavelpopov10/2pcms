<?
class Module_admin_controller extends _MODULE
{
	private static $model;
	private static $menu;
	private static $menucurr;
	private static $content;
	private static $calced = FALSE;
	private static $calcedm = FALSE;

	public function info() {return array('name' => 'Standart CP', 'version' => '0.1.2', 'author' => 'PavelPopov', 'compatible' => '0.1.7_Alpha_20140827_bferrtlf', 
	/* @todo: i18n компонентов */
	'components' => array(
		array('name' => 'Content', 'desc' => 'Use it to get current admin page content', 'return' => 'page content', 'link' => 'content'),
		array('name' => 'Menu', 'desc' => 'Use it to get current admin menu', 'return' => 'menu', 'link' => 'menu'),
	))+parent::info();}

	public function __construct()
	{
		if (!_REQUEST::isJustForInfo())
		{
			$this->checkacts();
			if (self::$model == null)
			{
				if ($this->inc('model.php')) {self::$model = new Module_admin_model();} else {trigger_error($this->getCorruptError(), E_USER_ERROR);}
			}
		}
	}
	
	public function menu()
	{
		if (!self::$calcedm)
		{
			self::$menu = $this->getMenu();
			self::$calcedm = true;
		}
		return self::$menu;
	}
	
	public function content()
	{
		if (!self::$calcedm)
		{
			self::$menu = $this->getMenu();
			self::$calcedm = true;
		}
		if (!self::$calced)
		{
			self::$content = $this->calcContent();
			self::$calced = true;
		}
		return self::$content;
	}
	
	private function calcContent()
	{
		$content = '';
		if (self::$menucurr == 'modules')
		{
			/* Получение валидированных модулей (установленных и не установленных вместе) */
			$dirs = array_filter(glob(MODPATH.'*'), array($this, 'checkMod'));
			/* Получение установленных модулей */
			$lmiarr = self::$model->getModules();
			if (!is_array($lmiarr))
			{
				return null;
			} else {
				foreach ($dirs as $dir)
				{
					/* @todo: пофиксить некошерный способ вычленения урла элемента здесь и ниже */
					$url = explode(DS, $dir);
					$url = $url[count($url)-1];
					
					$obj = null; $info = null;
					if (_REQUEST::inc_mod($url.DS))
					{
						$obj = _REQUEST::instantiate($url, true, $dir);
						$info = $obj->info();
					}
					if (!$obj instanceof _MODULE) {continue;}
					
					/* Проверка на присутствие в списке установленных модулей */
					$inval = null;
					foreach ($lmiarr as $val)
					{
						if ($val['url'] == $url) {$inval = $val;break;}
					}
					$installed = ($inval != null);
					$name = ($installed ? $inval['name'] : ($info != null ? $info['name'] : $url));
					$status = ($installed != null ? _MODULE::$_i18n['installed'] : _MODULE::$_i18n['not-installed']);
					$version = ($info != null ? $info['version'] : _MODULE::$_i18n['unknown']);
					$act = '<a href="?act='.($installed ? 'un' : '').'install&mod='.$url.'">'.($installed ? _MODULE::$_i18n['uninstall'] : _MODULE::$_i18n['install']).'</a>';
					/* admin.css класс tcpa - активная и tcpna - неактивная панель */
					$type = ($installed ? 'tcpa' : 'tcpna');
					$content .= $this->view('modules_list_element', array('type' => $type, 'link' => '?p=modules/'.$url, 'name' => $name,
					'abspath_label' => _MODULE::$_i18n['absolute_path'], 'abspath' => $dir,
					'version_label' => _MODULE::$_i18n['version'], 'version' => $version,
					'status_label' => _MODULE::$_i18n['status'], 'status' => $status,
					'act' => $act));
				}
				$this->title = _MODULE::$_i18n['controlpanel'];
			}
		} elseif (self::$menucurr == 'cfg') {
			$this->title = _MODULE::$_i18n['controlpanel'];
			$content = 'cfg';
		} elseif (is_array(self::$menucurr)) {
			$this->title = _MODULE::$_i18n['controlpanel'];
			$content = 'NAME:'.self::$menucurr['name'].' URL:'.self::$menucurr['url'];
		} else {
			/* Получение установленных модулей */
			$lmiarr = self::$model->getModules();
			$this->title = _MODULE::$_i18n['controlpanel'];
			$content = _MODULE::$_i18n['installed_modules_num'].': '.count($lmiarr);
		}
		return $content;
	}
	
	private function checkacts()
	{
		$act = '';
		if (isset($_POST['act'])) {$act = $_POST['act'];} elseif (isset($_GET['act'])) {$act = $_GET['act'];}
		switch($act)
		{
			case 'install':
				if (isset($_POST['mod'])) {$mod = $_POST['mod'];} elseif (isset($_GET['mod'])) {$mod = $_GET['mod'];} else {break;}
				_MODMAN::install(Core::$_db, $mod);
				header ('Location: ?p=modules');
				break;
			case 'uninstall':
				if (isset($_POST['mod'])) {$mod = $_POST['mod'];} elseif (isset($_GET['mod'])) {$mod = $_GET['mod'];} else {break;}
				_MODMAN::uninstall(Core::$_db, $mod);
				header ('Location: ?p=modules');
				break;
			default: break;
		}
	}
	
	private function getMenuItems($pname, $urlpre='', $urlprei='')
	{
		$menu = '';
		$lmiarr = self::$model->getModulesByParent($pname);
		if (!is_array($lmiarr))
		{
			return null;
		} else {
			$issetp = false;
			if (isset($_GET['p']))
			{
				$parr = explode('/', $_GET['p']);
				$carr = count($parr);
				$issetp = true;
			}
			/* Перебор полученных элементов меню */
			foreach ($lmiarr as $val)
			{			
				/* 8 - длина "modules" */
				if ($issetp && $urlpre != '')
				{
					$sub = (substr($_GET['p'], 8, strlen($urlpre)+1+strlen($val['url'])) == $urlpre.'/'.$val['url']);
				} elseif ($issetp) {
					$sub = (substr($_GET['p'], 8, strlen($val['url'])) == $val['url']);
				}
				if ($issetp && ($parr[$carr - 1] == $val['url'] && (($carr > 2) ? ($parr[$carr - 2] == $urlprei) : FALSE) || $sub))
				{
					if ($urlpre != '')
					{
						$menu .= '<li class="selected"><a href="?p=modules/'.$urlpre.'/'.$val['url'].'">'.$val['name'].'</a>';
					} else {
						$menu .= '<li class="selected"><a href="?p=modules/'.$val['url'].'">'.$val['name'].'</a>';
					}
					if ($parr[$carr - 1] == $val['url'] && (($carr > 2) ? ($parr[$carr - 2] == $urlprei) : TRUE))
					{
						self::$menucurr = $val;
					}
				} else {
					if ($urlpre != '')
					{
						$menu .= '<li><a href="?p=modules/'.$urlpre.'/'.$val['url'].'">'.$val['name'].'</a>';
					} else {
						$menu .= '<li><a href="?p=modules/'.$val['url'].'">'.$val['name'].'</a>';
					}
				}
				if ($urlpre != '')
				{
					$lmip = $this->getMenuItems($val['name'], $urlpre.'/'.$val['url'], $val['url']);
				} else {
					$lmip = $this->getMenuItems($val['name'], $val['url'], $val['url']);
				}
				if ($lmip != null)
				{
					$menu .= '<ul>'.$lmip.'</ul>';
				}
				$menu .= '</li>';
			}
		}
		return $menu;
	}
	
	/* Получение меню - долгий и рутинный процесс */
	private function getMenu()
	{
		$a = true;
		if (isset($_GET['p']))
		{
			$parr = explode('/', $_GET['p']);
			$carr = count($parr);
			self::$menucurr = $parr[0];
			if ($parr[0] == 'modules' || ($carr > 1 && $parr[1] == 'modules'))
			{
				$menua = '<li class="selected"><a href="?p=modules">'._MODULE::$_i18n['modules'].'</a>';
				$a = false;
			} else {
				$menua = '<li><a href="?p=modules">'._MODULE::$_i18n['modules'].'</a>';
			}
			$menui = $this->getMenuItems('');
			if ($menui != null)
			{
				$menua .= '<ul>'.$menui.'</ul>';
			}
			$menua .= '</li>';
			if ($_GET['p'] == 'cfg')
			{
				$menua .= '<li class="selected"><a href="?p=cfg">'._MODULE::$_i18n['cfg'].'</a></li>';
				$a = false;
			} else {
				$menua .= '<li><a href="?p=cfg">'._MODULE::$_i18n['cfg'].'</a></li>';
			}
			if ($a == false)
			{
				$menub = '<li><a href="'._REQUEST::getPage().'">'._MODULE::$_i18n['main'].'</a></li>';
			} else {
				$menub = '<li class="selected"><a href="'._REQUEST::getPage().'">'._MODULE::$_i18n['main'].'</a></li>';
			}
		} else {
			$menua = '<li><a href="?p=modules">'._MODULE::$_i18n['modules'].'</a>';
			$menui = $this->getMenuItems('');
			if ($menui === null)
			{
				$menua .= '</li>';
			} else {
				$menua .= '<ul>'.$menui.'</ul></li>';
			}
			$menua .= '<li><a href="?p=cfg">'._MODULE::$_i18n['cfg'].'</a>';
			$menub = '<li class="selected"><a href="'._REQUEST::getPage().'">'._MODULE::$_i18n['main'].'</a></li>';
		}
		return '<ul id="menu">'.$menub.$menua.'<li><a href="?act=logout">'._MODULE::$_i18n['logout'].'</a></li></ul>';
	}
	
	private function checkMod($var)
	{
		return (is_dir($var) && file_exists($var.DS.'controller.php'));
	}
}
?>