<?
class Module_auth_model
{
	public function getUserById($id)
	{
		$lmi = _PDO::q(Core::$_db, "SELECT * FROM `users` WHERE `id` = :id", array('id' => $id));
		if ($lmi === FALSE) {return FALSE;}
		return $lmi->fetch(PDO::FETCH_ASSOC);
	}
	
	public function getUserByLoginAndPass($login, $pass)
	{
		$lmi = _PDO::q(Core::$_db, "SELECT * FROM `users` WHERE `login` = :login AND `pass` = :pass", array('login' => $login, 'pass' => $pass));
		if ($lmi === FALSE) {return FALSE;}
		return $lmi->fetch(PDO::FETCH_ASSOC);
	}
}
?>