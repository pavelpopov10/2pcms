<?
class Module_auth_controller extends _MODULE
{
	private static $model;
	private static $checked = false;
	private static $authorised = false;
	public static $user = array('login' => '');
	private static $content;

	public function info() {return array('name' => 'Standart Auth', 'version' => '0.0.5', 'author' => 'PavelPopov', 'compatible' => '0.1.7_Alpha_20140827_bferrtlf', 
	/* @todo: i18n компонентов */
	'components' => array(
		array('name' => 'IsAuthorised', 'desc' => 'Use it to check if user authorised', 'return' => 'TRUE if authorised and FALSE otherwise', 'link' => 'bauth'),
		array('name' => 'Login', 'desc' => 'Use it to login', 'return' => 'TRUE if logged in and FALSE otherwise', 'link' => 'login'),
		array('name' => 'GetLogin', 'desc' => 'Use it to get login string if user was authorised', 'return' => 'login string if authorised and empty otherwise', 'link' => 'getlogin'),
		array('name' => 'GetError', 'desc' => 'Use it to get error string when user wasn\'t authorised as expected', 'return' => 'error string if error occured and empty string otherwise', 'link' => 'geterror'),
	),
	/* @todo: i18n фраз */
	'phrases' => array(
		array('name' => 'Login', 'desc' => 'Use it to get login phrase', 'return' => 'i18nd login phrase', 'link' => 'login'),
		array('name' => 'Pass', 'desc' => 'Use it to get pass phrase', 'return' => 'i18nd pass phrase', 'link' => 'password'),
		array('name' => 'LoginButton', 'desc' => 'Use it to get button phrase', 'return' => 'i18nd button phrase', 'link' => 'loginbtn')
	))+parent::info();}
	
	function __construct()
	{
		if (!_REQUEST::isJustForInfo())
		{
			if (self::$model == null)
			{
				if ($this->inc('model.php')) {self::$model = new Module_auth_model();} else {trigger_error($this->getCorruptError(), E_USER_ERROR);}
			}
			if (!self::$checked)	{self::$authorised = $this->auth(); self::$checked = TRUE;}
			if (self::$authorised)	{$this->checkacts();}
		}
	}

	public function login()
	{
		if (isset($_POST['login']) && isset($_POST['pass']))
		{
			if($_POST['login'] != '' && $_POST['pass'] != '')
			{
				$popass = _CRYPT::blowfish($_POST['pass']);
				$user = self::$model->getUserByLoginAndPass(Core::secure($_POST['login']), $popass);
				if (!empty($user))
				{
					if (!isset($_COOKIE['cookies_works']))
					{
						self::$content = _MODULE::$_i18n['turnoncookies'];
						return FALSE;
					} else {
						setcookie('cookies_works', '');
						$seid_i = (string) $user['id'];
						$seid_p = $popass;
						setcookie('seid', $seid_i.' '.$seid_p);
						header ('Location: /'._REQUEST::getPage());
					}
				} else {
					self::$content = _MODULE::$_i18n['wrongloginorpass'];
					return FALSE;
				}
			} else {
				self::$content = _MODULE::$_i18n['wrongloginorpass'];
				return FALSE;
			}
		} else {
			self::$content = '';
			return FALSE;
		}
	}
	
	public function bauth()
	{
		return self::$authorised;
	}
	
	public function geterror() {return self::$content;}
	public function getlogin() {return self::$user['login'];}
	
	private function checkacts()
	{
		$act = '';
		if (isset($_POST['act'])) {$act = $_POST['act'];} elseif (isset($_GET['act'])) {$act = $_GET['act'];}
		switch($act)
		{
			case 'logout':
				setcookie('seid', '');
				header ('Location: /'._REQUEST::getPage());
				break;
			default: break;
		}
	}
	
	private function auth()
	{
		if (isset($_COOKIE['seid']))
		{
			$seide = explode(' ', $_COOKIE['seid']);
			$id = (int) $seide[0];
			$pass = $seide[1];
			$user = self::$model->getUserById($id);
			if (!empty($user) && $user['pass'] == $pass)
			{
				self::$user = $user;
				return TRUE;
			} else {
				setcookie('seid', '');
				header ('Location: /'._REQUEST::getPage());
			}
		} else {
			if (!isset($_COOKIE['cookies_works'])) setcookie('cookies_works', 'yes');
			self::$content = '';
			return FALSE;
		}
	}
}
?>