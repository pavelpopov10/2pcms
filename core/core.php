<? defined('COREPATH') or die('ERROR [403]: No direct script access!');
/***************************************************
*	2PCMS by Pavel Popov © 2013
*	Расположение: ./core/core.php
*	Описание: Ядро
***************************************************/

/* Автозагрузчик классов ядра */
function core_classes($class){if(file_exists(CLASSPATH.$class.".php")) {return include_once(CLASSPATH.$class.".php");}}
spl_autoload_register('core_classes');

class Core
{
	/* (Версия сборки) [x.y.z] x = основная версия, y = вторичная версия, z = подверсия */
	const CMS_BUILD_VER = '0.1.7';
	/* (Стадия разработки сборки) N = NightBuild, PA = Pre-Alpha, A = Alpha, B = Beta, RC = Release Candidat, R = Release/Final */
	const CMS_BUILD_DEV = 'A';
	/* (Дата выхода сборки) [xxxxyyzz] x = год, y = месяц, z = день */
	const CMS_BUILD_DATE = '20140827';
	/* (Ключевое слово сборки) */
	const CMS_BUILD_KEY = 'bferrtlf';
	/* (Копирайт) 2PCMS by Pavel Popov © 2013. All rights reserved! (Do not change!) */
	const CMS_COPYRIGHT = '2PCMS by Pavel Popov © 2013-2014. All rights reserved!';

	/* Информация о клиенте */
	public static $_client = array();
	/* Конфигурация движка */
	public static $_cfg = array();
	/* Данные исполняемого модуля */
	public static $_module = array();
	/* Состояние работы ядра */
	public static $_working = FALSE;
	/* Подключение к СУБД */
	public static $_db = null;
	/* Системная локализация */
	public static $_i18n = array();
	
	/**
	 * Информация о сборке
	 * Пример: 0.0.1_NightBuild_20131227_kongo
	 *
	 * @return версию (w), стадию разработки (x), дату выхода (y) и ключевое слово (z) [w_x_y_z]
	 */
	public static function build_info()
	{
		if (!self::$_working){return;}
		switch(self::CMS_BUILD_DEV)
		{
			case 'N':$dev='NightBuild';break;case 'PA':$dev='PreAlpha';break;case 'A':$dev='Alpha';break;
			case 'B':$dev='Beta';break;case 'RC':$dev='RC';break;case 'R':$dev='Final';break;default:$dev='';break;
		}
		return self::CMS_BUILD_VER."_".$dev."_".self::CMS_BUILD_DATE."_".self::CMS_BUILD_KEY;
	}
	
	/**
	 * Обезвреживание строк
	 * Защищает от примитивных XSS атак.
	 *
	 * @return обезвреженную строку
	 */
	public static function secure($str)
	{
		return htmlspecialchars(trim($str));
	}
	
	public static function getconst($const)
	{
		return (defined($const)) ? constant($const) : null;
	}
	
	/**
	 * Запуск ядра в режиме установки
	 * 
	 */
	public static function install_launch()
	{		
		/* Не допускаем повторного запуска (Синглетон) */
		if (self::$_working){return;} self::$_working = TRUE;
		self::init_debug();
		
		/* Константы установщика */
		self::$_cfg['Language'] = 'ru';
		self::$_cfg['Theme'] = 'classic';
		self::$_cfg['ErrorHandling'] = 'on';
		self::$_cfg['ErrorHandling_show'] = 'on';
		self::$_cfg['ErrorHandling_logging'] = 'on';
		self::$_cfg['ErrorHandling_tracing'] = 'on';
		self::$_cfg['Logging'] = 'on';
		self::$_cfg['Logging_days'] = '7';
		self::$_cfg['Logging_place'] = 'files';
		
		/* Язык контента */
		if (isset($_COOKIE['sys_lang']) && strlen($_COOKIE['sys_lang']) == 2)
		{
			self::$_cfg['ContentLanguage'] = $_COOKIE['sys_lang'];
		} else {
			self::$_cfg['ContentLanguage'] = self::$_cfg['Language'];
		}
		
		/* Запускаем буфер вывода (если zlib есть, то сжимаем) */
		if (!ob_start("ob_gzhandler")) ob_start();
		
		self::init_headers();
		self::init_error_handlers();
		
		self::$_i18n = _I18N::getsl();
		
		/* Инициализация классов ядра */
		_ERR::init();
		
		self::get_client_info();
	}
	
	/**
	 * Запуск ядра
	 * 
	 */
	public static function launch()
	{
		/* Не допускаем повторного запуска (Синглетон) */
		if (self::$_working){return;} self::$_working = TRUE;
		self::init_debug();
		
		/* Загружаем конфиги */
		self::$_cfg = _INI::read(COREPATH.'mainconf.ini', FALSE);
		
		/* Язык контента */
		if (isset($_COOKIE['sys_lang']) && strlen($_COOKIE['sys_lang']) == 2)
		{
			self::$_cfg['ContentLanguage'] = $_COOKIE['sys_lang'];
		} else {
			self::$_cfg['ContentLanguage'] = self::$_cfg['Language'];
		}
		
		/* Запускаем буфер вывода (если zlib есть, то сжимаем) */
		if (self::$_cfg['PageCompression'] != 'on' || !ob_start("ob_gzhandler")) {ob_start();}

		self::init_headers();
		if (self::$_cfg['ErrorHandling'] == 'on') {self::init_error_handlers();}
		
		self::$_i18n = _I18N::getsl();
		/* Инициализация классов ядра */
		_ERR::init();
		
		/* Подключаемся к СУБД */
		Core::$_db = _PDO::c();
		
		self::get_client_info();
		
		/* Проверяем логи на свежесть */
		_LOG::checklogs();
		/* Обрабатываем запрос */
		_REQUEST::run();
	}
	
	/**
	 * Отключение ядра
	 * 
	 */
	public static function shutdown()
	{
		/* Когда ядро не запущено, нет смысла его выключать */
		if (!self::$_working){return;}
		if (self::$_cfg['ErrorHandling'] == 'on')
		{
			restore_error_handler();
			restore_exception_handler();
		}
		_ERR::shutdown();
		/* Отправляем (выводим) буфер */
		ob_end_flush();
		/* Выключаем ядро */
		self::$_working = FALSE;
	}
	
	public static function getRoot()
	{
		$roots = explode('/', $_SERVER['DOCUMENT_ROOT']);
		$froot = '';
		foreach ($roots as $root)
		{
			$froot .= $root.DS;
		}
		return $froot;
	}
	
	private static function init_debug()
	{
		/* Константы для отладки */
		if (!defined('2PCMS_START_TIME')){define('2PCMS_START_TIME', microtime(TRUE));}
		if (!defined('2PCMS_START_MEMORY')){define('2PCMS_START_MEMORY', memory_get_usage());}
	}
	
	private static function init_headers()
	{
		/* Раз каждая страница имеет в $_client свои параметры, нельзя кешировать их на прокси-сервере */
		ini_set("default_charset", "utf-8");
		header('X-Powered-By: 2PCMS '.self::build_info());
		header("Cache-control: private");
	}
	
	private static function init_error_handlers()
	{
		/* Устанавливаем обработчики ошибок и исключений */
		set_exception_handler(array('_ERR', 'exc'));
		set_error_handler(array('_ERR', 'err'));
		register_shutdown_function(array('Core', 'shutdown'));
	}
	
	private static function get_client_info()
	{
		/* Вытаскиваем информацию о клиенте из заголовков */
		self::$_client = [
			'ip' => empty($_SERVER['REMOTE_ADDR']) ? '0.0.0.0' : $_SERVER['REMOTE_ADDR'],
			'port' => empty($_SERVER['REMOTE_PORT']) ? '0000' : $_SERVER['REMOTE_PORT'],
			'browser_name' => self::user_browser(TRUE),
			'browser_version' => self::user_browser(FALSE),
			'request_type' => empty($_SERVER['REQUEST_METHOD']) ? 'FALSE' : $_SERVER['REQUEST_METHOD'],
			'request_uri' => empty($_SERVER['REQUEST_URI']) ? 'FALSE' : $_SERVER['REQUEST_URI'],
			'come_from' => empty($_SERVER['HTTP_REFERER']) ? 'FALSE' : $_SERVER['HTTP_REFERER'],
			'https' => empty($_SERVER['HTTPS']) ? 'FALSE' : $_SERVER['HTTPS']
		];
	}

	/**
	 * Определение браузера клиента
	 * 
	 * @param $name - если TRUE, возвращает название браузера. В обратном случае - версию.
	 * @return название или версию браузера клиента
	 */
	private static function user_browser($name)
	{
		$agent = $_SERVER['HTTP_USER_AGENT'];
		preg_match("/(MSIE|Opera|Firefox|Chrome|Version|Opera Mini|Netscape|Konqueror|SeaMonkey|Camino|Minefield|Iceweasel|K-Meleon|Maxthon)(?:\/| )([0-9.]+)/", $agent, $browser_info);
		list(,$browser,$version) = $browser_info;
		if (preg_match("/Opera ([0-9.]+)/i", $agent, $opera) || preg_match("/OPR\/([0-9.]+)/i", $agent, $opera)) if ($name){return 'Opera';}else{return $opera[1];}
		if ($browser == 'MSIE') {
			preg_match("/(Maxthon|Avant Browser|MyIE2)/i", $agent, $ie);
			if ($ie) {if ($name){return $ie[1].' based on IE';}else{return $version;}}
			if ($name){return 'IE';}else{return $version;}
		}
		if ($browser == 'Firefox') {
			preg_match("/(Flock|Navigator|Epiphany)\/([0-9.]+)/", $agent, $ff);
			if ($ff) {if ($name){return $ff[1];}else{return $ff[2];}}
		}
		if ($browser == 'Opera' && $version == '9.80') if ($name){return 'Opera';}else{return substr($agent,-5);}
		if ($browser == 'Version') if ($name){return 'Safari';}else{return $version;}
		if (!$browser && strpos($agent, 'Gecko')) if ($name){return 'Browser based on Gecko';}else{return $version;}
		if ($name){return $browser;}else{return $version;}
	}
}
?>