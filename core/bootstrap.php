<? defined('COREPATH') or die('ERROR [403]: No direct script access!');
/***************************************************
*	2PCMS by Pavel Popov © 2013
*	Расположение: ./core/init.php
*	Описание: Загрузчик ядра
***************************************************/

/* Проверяем на ошибки окружения */
if (headers_sent()) {die('WARNING!!! Some environment errors occurred, CORE was not launched!');}

/* Настраиваем окружение */
date_default_timezone_set('Europe/Moscow');
setlocale(LC_ALL, 'Russian_Russian.utf-8');

/* Подключаем ядро */
if (file_exists(COREPATH.'core.php')){include COREPATH.'core.php';}
else {die('ERROR [INIT_1]: No core found!');}

if (!isset($_POST['install_mode']))
{
	/* Запускаем ядро */
	Core::launch();
	/* Отключаем ядро */
	Core::shutdown();
} elseif ($_POST['install_mode'] == md5('goodidea')) {
	/* Запускаем ядро в режиме установки */
	Core::install_launch();
	return;
}
?>