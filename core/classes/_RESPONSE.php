<? defined('COREPATH') or die('ERROR [403]: No direct script access!');
/***************************************************
*	2PCMS by Pavel Popov © 2013
*	Расположение: ./core/classes/_RESPONSE.php
*	Описание: Обработчик вывода
***************************************************/
class _RESPONSE
{
	private static $locked = FALSE;
	private static $titlelock = FALSE;
	private static $mtlock = FALSE;
	private static $mimetype = 'text/html';
	private static $title = '';
	private static $script = '';
	private static $headtagin = '';
	private static $content = '';
	private static $css = array('');
	
	public static function setLocks($mtlock, $titlelock)
	{
		if (!self::$locked)
		{
			self::$mtlock = $mtlock;
			self::$titlelock = $titlelock;
			self::$locked = true;
		}
	}
	
	public static function setTitle($title)
	{
		if (!self::$titlelock)
		{
			self::$title = $title;
		}
	}
	
	public static function getTitle()
	{
		return self::$title;
	}
	
	public static function setMimeType($mimetype)
	{
		if (!self::$mtlock)
		{
			self::$mimetype = $mimetype;
		}
	}
	
	public static function getMimeType()
	{
		return self::$mimetype;
	}
	
	public static function addStyle($style)
	{
		self::$css[] = $style;
	}
	
	public static function addScript($script)
	{
		self::$script .= $script;
	}
	
	public static function getHeadContent()
	{
		return self::$headtagin;
	}
	
	public static function setHeadContent($con)
	{
		self::$headtagin = $con;
	}
	
	public static function send($content)
	{
		self::$content = $content;
		header("Content-type: ".self::$mimetype);
		/* Проверка на html мимтип. Если нет - отправляем просто контент. (Например отправка картинки, полученной путём file_get_contents()) */
		if (self::$mimetype == 'text/html')
		{
		echo '
<!DOCTYPE html>
<html>
    <head>
        <title>'.self::$title.'</title>
		';
		echo '<script>'.self::$script.'</script>
		';
		foreach (self::$css as $c)
		{
			echo '<link rel="stylesheet" type="text/css" href="http://'.Core::$_cfg['DomainName'].'/'.$c.'theme.css" media="screen" />
			';
		}
		echo self::$headtagin;
		echo '
    </head>
    <body>
		'.self::$content.'
    </body>
</html>
		';
		} else {
			print self::$content;
		}
	}
}
?>