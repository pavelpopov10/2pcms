<? defined('COREPATH') or die('ERROR [403]: No direct script access!');
/***************************************************
*	2PCMS by Pavel Popov © 2013
*	Расположение: ./core/classes/Logger.php
*	Описание: Логгер
***************************************************/
 
class _LOG 
{
	/**
	 * Запись в лог
	 * 
	 * @param $text - текст записи.
	 */
	public static function write($text)
	{
		if (Core::$_cfg['Logging'] == 'on')
		{
			$fname = LOGPATH."log_".date('Y_m_d').".txt";
			file_put_contents($fname, date('[H:i:s] ').$text.PHP_EOL, FILE_APPEND);
		}
	}
	
	/**
	* Получение последней записи
	*
	*/
	public static function readlast()
	{
		$line = '';

		$yi = 0;
		$filepath = '';
		while (true)
		{
			$filepath = LOGPATH."log_".date('Y_m_d', mktime(0,0,0,date("m"),date("d")-$yi,date("Y"))).".txt";
			if (file_exists($filepath))
			{
				break;
			} else if ($yi > 365) {
				return null;
			}
			$yi++;
		}
		
		$f = fopen($filepath, 'r');
		$cursor = -1;

		fseek($f, $cursor, SEEK_END);
		$char = fgetc($f);

		/* Обрезание всех символов перевода строки */
		while ($char === "\n" || $char === "\r") {
			fseek($f, $cursor--, SEEK_END);
			$char = fgetc($f);
		}

		/* Чтение до конца файла или до символа перевода строки */
		while ($char !== false && $char !== "\n" && $char !== "\r") {
			$line = $char . $line;
			fseek($f, $cursor--, SEEK_END);
			$char = fgetc($f);
		}

		return substr($line, 11, strlen($line));
	}
	
	/**
	 * Удаление устаревших записей
	 * 
	 */
	public static function checklogs()
	{
		$res = glob(LOGPATH."log_".date("Y_m_d", strtotime('-'.Core::$_cfg['Logging_days'].' days')).".[tT][xX][tT]");
		if (is_array($res))
		{
			array_map('unlink', $res);
		}
	}
	
	/**
	 * Удаление всех логов
	 * 
	 */
	public static function cleanlogs()
	{
		array_map('unlink', glob(LOGPATH."log_[0-9][0-9][0-9][0-9]_[0-9][0-9]_[0-9][0-9].[tT][xX][tT]"));
	}
}
 
?>