<? defined('COREPATH') or die('ERROR [403]: No direct script access!');
/***************************************************
*	2PCMS by Pavel Popov © 2013
*	Расположение: ./core/classes/_MODMAN.php
*	Описание: Менеджер модулей
***************************************************/
class _MODMAN
{
	/**
	* Устанавливает модуль
	*
	* @param $db - дескриптор подключения к СУБД
	* @param $path - путь до модуля относительно директории модулей
	* @see _PDO
	*/
	public static function install($db, $path)
	{
		if (!Core::$_working){return;}
		$f = FALSE;
		_LOG::write("[MODMAN] Trying to install module ".$path);
		
		$abs = MODPATH.$path.DS;
		$sql = $abs."install.sql";
		if (!file_exists($sql) || !$sqlq = file_get_contents($sql)) {
			$sqlq = '';
		}
		
		$obj = null; $info = null;
		if (!_REQUEST::inc_mod($path.DS))
		{
			$f = TRUE;
		}
		if ($f == FALSE)
		{
			$obj = _REQUEST::instantiate($path, true, $path.DS);
			$info = $obj->info();
			$name = $info['name'];
			$pname = '';

			/* Не забыть изменить $path с учётом $pname */
			$sqlq .= "INSERT INTO `modules` (`name`, `pname`, `url`) VALUES (:name, :pname, :path)";
			if(_PDO::t($db, $sqlq, array('name' => $name, 'pname' => $pname, 'path' => $path)) === FALSE) {$f = TRUE;}
		}
	
		_LOG::write("[MODMAN] Module ".$path.($f?' was not':'')." installed");
		return !$f;
	}
	
	/**
	* Деинсталлирует модуль
	*
	* @param $db - дескриптор подключения к СУБД
	* @param $path - путь до модуля относительно директории модулей
	* @see _PDO
	*/
	public static function uninstall($db, $path)
	{
		if (!Core::$_working){return;}
		$f = FALSE;
		_LOG::write("[MODMAN] Trying to uninstall module ".$path);
		
		$abs = MODPATH.$path.DS;
		$sql = $abs."uninstall.sql";
		if (!file_exists($sql) || !$sqlq = file_get_contents($sql)) {
			$sqlq = '';
		}
		
		$obj = null; $info = null;
		if (!_REQUEST::inc_mod($path.DS))
		{
			$f = TRUE;
		}
		if ($f == FALSE)
		{
			$obj = _REQUEST::instantiate($path, true, $path.DS);
			$info = $obj->info();
			$name = $info['name'];

			/* Не забыть изменить $path с учётом $pname */
			$sqlq .= "DELETE FROM `modules` WHERE `name` = :name OR `pname` = :name";
			if(_PDO::t($db, $sqlq, array('name' => $name)) === FALSE) {$f = TRUE;}
		}
		
		_LOG::write("[MODMAN] Module ".$path.($f?' was not':'')." uninstalled");
		return !$f;
	}
	
	/**
	* Проверяет установлен ли модуль
	*
	* @param $modname - имя модуля
	* @return TRUE, если модуль установлен
	*/
	public static function isModuleInstalled($modname)
	{
		$lmi = _PDO::q(Core::$_db, "SELECT * FROM `modules` WHERE `name` = :modname", array('modname' => $modname));
		if ($lmi === FALSE) {return null;}
		if (is_array($lmi->fetch()))
		{
			return TRUE;
		}
		return FALSE;
	}
	
	/**
	* Получает все установленные и присутствующие модули
	*
	* @return массив, содержащий в себе имена модулей, которые были установлены и существуют в директории модулей
	*/
	public static function getInstalledModules()
	{
		/* Получение валидированных модулей (установленных и не установленных вместе) */
		$dirs = array_filter(glob(MODPATH.'*'), array($this, 'checkMod'));
		/* Получение установленных модулей */
		$lmi = _PDO::q(Core::$_db, "SELECT * FROM `modules`");
		if ($lmi === FALSE) {return FALSE;}
		$lmiarr = $lmi->fetchAll();
		if (!is_array($lmiarr))
		{
			return null;
		} else {
			$result = array();
			foreach ($dirs as $dir)
			{
				/* @todo: пофиксить некошерный способ вычленения урла элемента здесь и ниже */
				$url = explode(DS, $dir);
				$url = $url[count($url)-1];
				
				/* Проверка на присутствие в списке установленных модулей */
				$inval = null;
				foreach ($lmiarr as $val)
				{
					if ($val['url'] == $url) {$inval = $val;break;}
				}
				if ($inval != null) {$result[] = $inval['name'];}
			}
			return $result;
		}
	}
	
	private function checkMod($var)
	{
		return (is_dir($var) && file_exists($var.DS.'controller.php'));
	}
}
?>