<? defined('COREPATH') or die('ERROR [403]: No direct script access!');
/***************************************************
*	2PCMS by Pavel Popov © 2013
*	Расположение: ./core/classes/_INI.php
*	Описание: Упрощение работы с INI-файлами
***************************************************/

class _INI
{
	
	/**
	 * Чтение INI-файла в массив
	 *
	 * @param $path - путь к файлу
	 * @param $has_sections - имеет ли INI-файл секции. По умолчанию - FALSE.
	 * @return ассоциативный массив с данными
	 */
	public static function read($path, $has_sections=FALSE) {return parse_ini_file($path, $has_sections);}
	
	/**
	 * Запись INI-файла из массива
	 *
	 * @param $assoc_arr - ассоциативный массив с данными для записи
	 * @param $path - путь к файлу
	 * @param $has_sections - имеет ли INI-файл секции. По умолчанию - FALSE.
	 * @return TRUE, если всё прошло успешно, и FALSE в обратном случае.
	 */
	public static function write($assoc_arr, $path, $has_sections=FALSE, $description='')
	{
		if ($description == '')
		{
				$content = ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	".Core::CMS_COPYRIGHT."
;	".Core::$_i18n['place'].": $path
;	".Core::$_i18n['auto']."
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n";
		} else {
				$content = ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	".Core::CMS_COPYRIGHT."
;	".Core::$_i18n['place'].": $path
;	".Core::$_i18n['desc'].": $description
;	".Core::$_i18n['auto']."
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n";
		}
		if ($has_sections)
		{
			foreach ($assoc_arr as $key=>$elem)
			{
				$content .= "[".$key."]\n";
				foreach ($elem as $key2=>$elem2)
				{
					if(is_array($elem2))
					{
						for($i=0;$i<count($elem2);$i++)
						{
							if (is_string($elem2[$i])) {
								$content .= $key2."[] = \"".$elem2[$i]."\"\n";
							} elseif ($elem2[$i] === TRUE) {
								$content .= $key2."[] = 1\n";
							} else if ($elem2[$i] === FALSE) {
								$content .= $key2."[] = 0\n";
							} else {
								$content .= $key2."[] = ".$elem2[$i]."\n";
							}
						}
					}
					else if($elem2=="") $content .= $key2." = \n";
					else $content .= $key2." = \"".$elem2."\"\n";
				}
			}
		} else {
			foreach ($assoc_arr as $key=>$elem) {
				if(is_array($elem))
				{
					for($i=0;$i<count($elem);$i++)
					{
							if ($elem[$i] === TRUE)
							{
								$content .= $key."[] = TRUE\n";
							} else if ($elem[$i] === FALSE) {
								$content .= $key."[] = FALSE\n";
							} else if (is_string($elem[$i])) {
								$content .= $key."[] = \"".$elem[$i]."\"\n";
							} else {
								$content .= $key."[] = ".$elem[$i]."\n";
							}
					}
				}
				else if($elem=="") $content .= $key." = \n";
				else $content .= $key." = \"".$elem."\"\n";
			}
		}
		if (!$handle = fopen($path, 'w')) {return false;} 
		if (!fwrite($handle, $content)) {fclose($handle);return false; } 
		fclose($handle);
		return true; 
	}
}
?>