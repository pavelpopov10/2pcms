<? defined('COREPATH') or die('ERROR [403]: No direct script access!');
/***************************************************
*	2PCMS by Pavel Popov © 2013
*	Расположение: ./core/classes/_PDO.php
*	Описание: Упрощение работы с PDO
***************************************************/

class _PDO extends PDO
{
	/* Выбранный драйвер СУБД */
	public static $driver;
	/* Данные подключения */
	private static $dbconf = array();
	
	/**
	 * Подключение к СУБД
	 * 
	 * @return TRUE, если подключение успешно настроено, и FALSE в обратном случае.
	 * @throw PDOException
	 */
	public static function c()
	{
		self::$dbconf = _INI::read(COREPATH.'dbconf.ini');
		self::$driver = self::$dbconf['driver'];
		if (self::$driver == "mysql")
		{
			$dsn = "mysql:host=".self::$dbconf['host'].";dbname=".self::$dbconf['dbname'].";port=".self::$dbconf['port'].";charset=".self::$dbconf['charset'];
		} else {
			return FALSE;
		}
		/*
		switch (self::$driver)
		{
			case "mysql": $dsn = "mysql:host=".self::$dbconf['host'].";dbname=".self::$dbconf['dbname'].";port=".self::$dbconf['port'].";charset=".self::$dbconf['charset']; break;
			default: return FALSE;
		}
		*/
		try {
			$db = new PDO($dsn, self::$dbconf['usrname'], self::$dbconf['passw']);
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $db;
		} catch (PDOException $e) {throw $e;}
	}

	/**
	 * Запрос к СУБД (Не транзакция)
	 * 
	 * @throw PDOException
	 */
	public static function q($db, $sql, $arr=Array())
	{
		try {
			$sth = $db->prepare($sql);
			foreach($arr as $key => $value) {$sth->bindValue(':'.$key, $value);}
			$sth->execute();
			return $sth;
		} catch (PDOException $e) {throw $e;}
	}
	
	/**
	 * Транзакция к СУБД
	 * 
	 * @throw PDOException
	 */
	public static function t($db, $sql, $arr=Array())
	{
		try {
			$db->beginTransaction();
			$sth = $db->prepare($sql);
			foreach($arr as $key => $value) {$sth->bindValue(':'.$key, $value);}
			$sth->execute();
			$db->commit();
			return $sth;
		} catch (PDOException $e) {$db->rollback(); throw $e;}
	}
}
?>