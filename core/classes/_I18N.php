<? defined('COREPATH') or die('ERROR [403]: No direct script access!');
/***************************************************
*	2PCMS by Pavel Popov © 2013
*	Расположение: ./core/classes/_I18N.php
*	Описание: Интернационализация
***************************************************/
class _I18N
{
	/*
	 * Интернационализация системы
	 *
	 */
	public static function getsl()
	{
		return _INI::read(I18NPATH.Core::$_cfg['Language'].'.ini', FALSE);
	}
	
	/*
	 * Интернационализация контента
	 *
	 */
	public static function getcl($modulepath)
	{
		$file = MODPATH.$modulepath.'i18n'.DS.Core::$_cfg['ContentLanguage'].'.ini';
		if (!file_exists($file)) {return FALSE;}
		return _INI::read($file, FALSE);
	}
}