<? defined('COREPATH') or die('ERROR [403]: No direct script access!');
/***************************************************
*	2PCMS by Pavel Popov © 2013
*	Расположение: ./core/classes/_CRYPT.php
*	Описание: Криптография, шифрование данных
***************************************************/

class _CRYPT
{
	private static $iterations = 512;
	
	/**
	* Хеширование данных посредством crypt
	*
	* @param $s - исходная строка
	* @param $its - кол-во дополнительных раундов
	* @param $presalt - начало соли
	* @param $saltlength - длина генерируемой соли
	*/
	public static function rcrypt($s, $its, $presalt, $saltlength)
	{
		$result = $s;
		$ss = 0;
		if (!empty($s))
		{
			foreach (str_split($s) as $chr) {$ss .= ord($chr);}
		} else {$ss = 128;}
		$saltsyms = "./ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for ($it=0;$it<$its;$it++)
		{
			$roundsalt = '';
			for ($sw=0;$sw<$saltlength;$sw++)
			{
				$symnum = strlen($s)*$ss+$it*2+($saltlength*4-$sw*3);
				$roundsalt .= $saltsyms{abs($symnum%64)};
			}
			$result = crypt($result, $presalt.$roundsalt);
		}
		return $result;
	}

	/**
	* Хеширование данных с использованием алгоритма BlowFish
	*
	* @param $s - исходная строка
	*/
	public static function blowfish($s)
	{
		return self::rcrypt($s, self::$iterations, '$2a$04$', 22);
	}
}
?>