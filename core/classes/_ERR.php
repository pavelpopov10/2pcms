<? defined('COREPATH') or die('ERROR [403]: No direct script access!');
/***************************************************
*	2PCMS by Pavel Popov © 2013
*	Расположение: ./core/classes/_ERR.php
*	Описание: Обработчик ошибок
***************************************************/
class _ERR
{
	private static $i18n = array();

	public static function init()
	{
		if (!Core::$_working) {return;}
		foreach (Core::$_i18n as $key => $val)
		{
			if (Core::getconst($key) != NULL) {self::$i18n[constant($key)] = $val; unset(self::$i18n[$key]);}
		}
	}
	
	/**
	 * Конвертер ошибок в исключения
	 *
	 * @return TRUE, если всё прошло успешно.
	 */
	public static function err($code, $error, $file = NULL, $line = NULL)
	{
		self::exc(new ErrorException($error, $code, 0, $file, $line));
		return true;
	}
	
	/**
	 * Обработчик исключений
	 *
	 * @param Exception $e - обрабатываемое исключение
	 */
	public static function exc(Exception $e)
	{
		$class   = get_class($e);
		$code    = $e->getCode();
		$message = $e->getMessage();
		$file    = $e->getFile();
		$line    = $e->getLine();
		$tracert = $e->getTrace();		
		if (Core::$_cfg['ErrorHandling_show'] == 'on')
		{
			/* TODO: Как-то уже упростить эту конструкцию */
			$color = 'black';
			if ($code == E_ERROR || $code == E_PARSE || $code == E_CORE_ERROR || $code == E_COMPILE_ERROR) {$color = 'red';}
			if ($code == E_WARNING || $code == E_CORE_WARNING || $code == E_COMPILE_WARNING || $code == E_RECOVERABLE_ERROR) {$color = 'brown';}
			if ($code == E_USER_ERROR || $code == E_USER_WARNING || $code == E_USER_NOTICE || $code == E_USER_DEPRECATED) {$color = 'blue';}
			if ($code == E_NOTICE || $code == E_STRICT || $code == E_DEPRECATED) {$color = 'green';}
			if (empty(self::$i18n[$code])) { $php_error_type = Core::$_i18n['errorstr']." ".$code; } else { $php_error_type = "<b style='color: $color'>".self::$i18n[$code]."</b>"; }
			echo "<div class='exception'><hr>[".$php_error_type." ".Core::$_i18n['errorin']." <b class='exception_class'>$class</b> ($file) ".Core::$_i18n['errorline']." <b class='exception_line'>$line</b>]: ".strip_tags($message);
			if (Core::$_cfg['ErrorHandling_tracing'] == 'on')
			{
				$trace = Core::$_i18n['errortrace'].":";
				foreach ($tracert as $val)
				{
					if (!empty($val['args']))
					{
						$args = $val['args'][0];
						foreach ($val['args'] as $arg)
						{
							$args = ", ".$arg;
						}
					} else {
						$args = "";
					}
					$trace .= "~ ".$val['file']." [".$val['line']."] - ".$val['function']."(".$args.");<br>";
				}
				echo "<p class='exception_trace'>$trace</p>";
			}
			echo "<hr></div>";
		} elseif ($code == E_ERROR || $code == E_PARSE || $code == E_COMPILE_ERROR || $code == E_CORE_ERROR || $code == E_USER_ERROR) {
			/* Очищаем буффер (не выводим стандартное сообщение об ошибке) */
			ob_get_level() AND ob_clean();
			echo self::$i18n['out'];
		}
		if (Core::$_cfg['ErrorHandling_logging'] == 'on')
		{
			if (empty(self::$i18n[$code])) { $php_error_type = Core::$_i18n['errorstr']." ".$code; } else { $php_error_type = self::$i18n[$code]; }
			$error = "[$php_error_type ".Core::$_i18n['errorin']." $class ($file) ".Core::$_i18n['errorline']." $line]: ".strip_tags($message);
			if (Core::$_cfg['ErrorHandling_tracing'] == 'on')
			{
				$trace = PHP_EOL."   ".Core::$_i18n['errortrace'].":";
				foreach ($tracert as $val)
				{
					$trace .= "   ~".trim($val).PHP_EOL;
				}
				$error .= $trace;
			}
			_LOG::write($error);
		}
	}
	/**
	 * Проверка на фатальные ошибки при выключении ядра
	 *
	 */
	public static function shutdown()
	{
		if ($error = error_get_last() AND $error['type'] & ( E_ERROR | E_PARSE | E_COMPILE_ERROR | E_CORE_ERROR))
		{
			/* Очищаем буффер (не выводим стандартное сообщение об ошибке) */
			ob_get_level() AND ob_clean();
			/* Запускаем обработчик ошибок */
			self::err($error['type'], $error['message'], $error['file'], $error['line']);
			exit(1);
		}
	}
}
?>