<? defined('COREPATH') or die('ERROR [403]: No direct script access!');
/***************************************************
*	2PCMS by Pavel Popov © 2013
*	Расположение: ./core/classes/_MODULE.php
*	Описание: База модулей
***************************************************/
class _MODULE
{
	public static $_i18n;

	/**
	 * Информация о модуле
	 *
	 */
	protected function info() {return array('name' => 'UNKNOWN', 'version' => 'UNKNOWN', 'author' => 'UNKNOWN', 'compatible' => Core::build_info());}
	
	/**
	 * Парсинг вида модуля
	 * 
	 * @param $path - путь до файла вида относительно директории видов модуля
	 * @param $arr - ассоциативный массив с данными для парсинга. По умолчанию массив пуст и парсинг не проводится.
	 * @return обработанное содержание шаблона (вида).
	 */
	protected function view($path, $arr=array())
	{
		if (!Core::$_working){return;}
		if (!file_exists(Core::$_module['syspath'].'view'.DS.$path.'.php')) {trigger_error('ERROR [MODULE]: '.Core::$_i18n['noviewfound'].' '.Core::$_module['syspath'].'view'.DS.$path.'.php'.'!', E_USER_ERROR);}
		$content = file_get_contents (Core::$_module['syspath'].'view'.DS.$path.'.php');
		foreach ($arr as $key => $value) {$content = str_replace ('<!-- '.$key.' -->', $value, $content);}
		return $content;
	}
	
	/**
	 * Подключение стороннего скрипта
	 * 
	 * @param $path - путь до подключаемого файла относительно директории модуля
	 * @return TRUE, если всё прошло успешно, и FALSE в обратном случае.
	 */
	protected function inc($path)
	{
		if (!Core::$_working){return;}
		if (!file_exists(Core::$_module['syspath'].$path)) {trigger_error('ERROR [MODULE]: '.Core::$_i18n['noincfound'].' '.Core::$_module['syspath'].$path.'!', E_USER_WARNING); return FALSE;}
		include_once (Core::$_module['syspath'].$path);
		return TRUE;
	}
	
	/**
	 * Получение интернационализации
	 * 
	 * @param $name - путь до подключаемого файла относительно директории интернационализации модуля (i18n)
	 * @return массив строк интернационализации, если всё прошло успешно, и FALSE в обратном случае.
	 */
	protected function geti18n($name)
	{
		if (!Core::$_working){return;}
		if (!file_exists(Core::$_module['syspath'].'i18n'.DS.Core::$_cfg['ContentLanguage'].DS.$name.'.ini')) {trigger_error('ERROR [MODULE]: '.Core::$_i18n['noi18nfound'].' '.Core::$_module['syspath'].'i18n'.DS.Core::$_cfg['ContentLanguage'].DS.$name.'.ini'.'!', E_USER_WARNING); return FALSE;}
		return _I18N::getcl(Core::$_module['syspath'], $name);
	}
	
	/**
	 * Получение содержания ошибки повреждения модуля
	 * 
	 */
	protected function getCorruptError()
	{
		if (!Core::$_working){return;}
		return 'ERROR [MODULE]: '.Core::$_i18n['module'].' '.Core::$_module['sysname'].' ('.Core::$_module['syspath'].') '.Core::$_i18n['corrupted'];
	}
	
	/**
	 * Получение содержания ошибки повреждения модуля
	 * 
	 */
	protected function getDependencyError()
	{
		if (!Core::$_working){return;}
		return 'ERROR [MODULE]: '.Core::$_i18n['module'].' '.Core::$_module['sysname'].' ('.Core::$_module['syspath'].') '.Core::$_i18n['dependency'];
	}
}
?>