<? defined('COREPATH') or die('ERROR [403]: No direct script access!');
/***************************************************
*	2PCMS by Pavel Popov © 2013
*	Расположение: ./core/classes/_REQUEST.php
*	Описание: Обработчик запроса
***************************************************/
class _REQUEST
{
	private static $jfinfo = false;
	private static $uri_imp_ds = '';
	private static $uri_imp_slash = '';
	private static $page = '';
	
	/**
	 * Получение URL
	 * 
	 * @param $ds - если TRUE, то возвращает URL, разделённый системным символом разделения каталогов... (Windows - \)
	 */
	public static function getUriImp($ds)
	{
		return ($ds?self::$uri_imp_ds:self::$uri_imp_slash);
	}
	/**
	 * Возвращает текущую страницу без прекрас
	 * 
	 */
	public static function getPage()
	{
		return self::$page;
	}
	
	public static function run()
	{
		if(!Core::$_working){return;}
		$curr_route = '';
		/* Разбор URI */
		$uri = array_slice(explode('/', Core::$_client['request_uri']), 1);
		self::$uri_imp_ds = implode(DS, $uri);
		self::$uri_imp_slash = implode('/', $uri);
		/* Получение списка страниц */
		$routes = _INI::read(TPLPATH.'routes.ini', true);
		/* Получение текущей страницы из списка */
		foreach ($routes as $regexp => $arr)
		{
			$slashwogets = explode('?', self::$uri_imp_slash);
			$slashwoget = (count($slashwogets) == 2?$slashwogets[0]:self::$uri_imp_slash);
			if ($regexp == "index" && ($slashwoget == "" || $slashwoget == $regexp)) {
				self::$page = $slashwoget;
				$curr_route = $routes[$regexp];
				break;
			} else if ($regexp == "" && $slashwoget == "404") {
				self::$page = $slashwoget;
				$curr_route = $routes[$regexp];
				break;
			} else if (preg_match('/'.$regexp.'/i', self::$uri_imp_slash, $matches)) {
				self::$page = $matches[0];
				$curr_route = $routes[$regexp];
				break;
			}
		}
		if ($curr_route == '') {self::error404();}
		
		/* Получение имени шаблона */
		$theme = Core::$_cfg['Theme'];
		
		$uri_to = Core::$_cfg['DomainName'].'/'.self::$uri_imp_slash;
		
		$gext = array('bmp','dib','gif','ico','jpg','jpeg','png','psd','tga','tgpic','tif','tiff');
		$gmt = array('image/bmp','image/bmp','image/gif','image/vnd.microsoft.icon','image/jpeg','image/jpeg',
		'image/png','image/vnd.adobe.photoshop','image/x-targa','image/x-tga','image/tiff','image/tiff');
		
		$iext = array('css','htm','html','js','eot','svg','svgz','ttf','woff');
		$imt = array('text/css','text/html','text/html','text/javascript','application/vnd.ms-fontobject','image/svg+xml','image/svg+xml','font/ttf','application/font-woff');
		
		$dots = explode('.', self::$uri_imp_slash);
		$ext = $dots[count($dots)-1];
		
		/* Редирект фавикона и заглушка + вывод стилей, картинок и доступных для вывода материалов из includes */
		if (self::$uri_imp_slash == 'favicon.ico')
		{
			if (file_exists(MEDIAPATH.'favicon.ico'))
			{
				_RESPONSE::setMimeType("image/vnd.microsoft.icon");
				_RESPONSE::send(file_get_contents(MEDIAPATH.'favicon.ico'));
			}
			return;
		} else if (self::$uri_imp_slash == 'theme.css') {
			_RESPONSE::setMimeType("text/css");
			if (file_exists(TPLPATH.$theme.DS.'theme.css'))
			{
				_RESPONSE::send(file_get_contents(TPLPATH.$theme.DS.'theme.css'));
			}
			return;
		} else if (in_array($ext, $gext)) {
			if (file_exists(MEDIAPATH.self::$uri_imp_ds))
			{
				for ($i=0;$i<count($gext);$i++)
				{
					if ($ext == $gext[$i])
					{
						_RESPONSE::setMimeType($gmt[$i]); break;
					}
				}
				if (file_exists(MEDIAPATH.self::$uri_imp_ds))
				{
					_RESPONSE::send(file_get_contents(MEDIAPATH.self::$uri_imp_ds));
				}
			}
			return;
		} else if (substr(self::$uri_imp_slash, -9, strlen(self::$uri_imp_slash)) == 'theme.css') {
			$st = substr(self::$uri_imp_ds, 0, -9);
			_RESPONSE::setMimeType("text/css");
			if (file_exists(MODPATH.$st.'themes'.DS.$theme.'.css'))
			{
				_RESPONSE::send(file_get_contents(MODPATH.$st.'themes'.DS.$theme.'.css'));
			}
			return;
		} else if (substr(self::$uri_imp_slash, 0, 4) == 'inc/') {
			if (in_array($ext, $iext))
			{
				for ($i=0;$i<count($iext);$i++)
				{
					if ($ext == $iext[$i])
					{
						_RESPONSE::setMimeType($imt[$i]); break;
					}
				}
				$file = INCPATH.substr(self::$uri_imp_ds, 4, strlen(self::$uri_imp_ds));
				if (file_exists($file))
				{
					_RESPONSE::send(file_get_contents($file));
				}
			}
			return;
		}
		
		$log_record_socket = 'IP:'.Core::$_client['ip'].':'.Core::$_client['port'];
		$log_record = $log_record_socket.'|BROWSER:'.Core::$_client['browser_name'].'_'.Core::$_client['browser_version'].'|METHOD:'.Core::$_client['request_type'].'|HTTPS:'.Core::$_client['https'].'|FROM:'.Core::$_client['come_from'].'|TO:'.$uri_to;
		$last_record = _LOG::readlast();
		$last_record_elems = explode('|', $last_record);
		$last_record_socket = substr($last_record_elems[0],3,strlen($last_record_elems[0]));
		
		if (Core::$_client['ip'] != explode(':', $last_record_socket)[0] || substr($log_record, strlen($log_record_socket), strlen($log_record)) != substr($last_record, strlen($last_record_elems[0]), strlen($last_record)))
		{
			_LOG::write($log_record);
		}
		
		_RESPONSE::setTitle($curr_route['title']);
		_RESPONSE::setLocks(($curr_route['mimetype_lock'] == 'on'), ($curr_route['title_lock'] == 'on'));
		
		/* Получение html */
		$pagecontent = file_get_contents (TPLPATH.$theme.DS.Core::$_cfg['ContentLanguage'].DS.$curr_route['page'].'.php');
		/* Парсинг переменных страницы в шаблоне (Страничные включения) */
		// foreach ($curr_route as $key => $value)
		// {
			// $pagecontent = str_replace ('<!--{'.$key.'}-->', $value, $pagecontent);
		// }
		
		$pagecontent = self::handle($pagecontent);
		
		$pagecontent = self::replacesysvars($pagecontent);
		
		/* Пример: {-/index/-} */
		while (($stpss = strpos($pagecontent, '{-/')) !== FALSE)
		{
			$t = $stpss+3;
			if (($stpse = strpos($pagecontent, '/-}', $t)) === FALSE)
			{
				/* @todo: i18n */
				trigger_error('ERROR [TPL_SYNTAX]: Redirect end not found! (Starts at: '.$stpss.')', E_USER_ERROR); self::error404();
			}
			$val = substr($pagecontent, $t, $stpse-$t);
			if (!headers_sent())
			{
				header("Location: /".$val);
				Core::shutdown();
			} else {
				/* @todo: i18n */
				_RESPONSE::setMimeType("text/html");
				_RESPONSE::send('Перенаправление невозможно - нажмите <a href="/'.$val.'">сюда</a> самостоятельно!');
				Core::shutdown();
			}
		}
		_RESPONSE::send($pagecontent);
	}
	
	public static function replacesysvars($source)
	{
		$source = str_replace ('{PAGE}', self::getPage(), $source);
		$source = str_replace ('{DOMAIN}', Core::$_cfg['DomainName'], $source);
		return $source;
	}
	
	private static function checki($source, $delimiter)
	{
		$off = 0;
		$dellen = strlen($delimiter);
		while (($end = strpos($source, $delimiter, $off)) !== FALSE)
		{
			$part = substr($source, 0, $end);
			$startscount = substr_count($part, '{[');
			$endscount = substr_count($part, ']}');
			$off = $end+$dellen;
			if ($startscount <= $endscount)
			{
				return array($part, $off);
			}
		}
		return null;
	}
	
	private static function searchcond($source, $trigerror)
	{
		$result = $source;
		$startfound = false;
		$endfound = false;
		while (($start = strpos($result, '{[')) !== FALSE)
		{
			$startfound = true;
			$so = $start+2;
			$endoff = $so;
			while (($end = strpos($result, ']}', $endoff)) !== FALSE)
			{
				$in = substr($result, $so, $end-$so);
				$startscount = substr_count($in, '{[');
				$endscount = substr_count($in, ']}');
				if ($startscount == $endscount)
				{
					$endfound = true;
					$inlen = strlen($in);
					
					/* если */
					$cond = self::checki($in, ']?[');
					if ($cond === null)
					{
						/* @todo: i18n */
						if ($trigerror) {trigger_error('ERROR [TPL_SYNTAX]: Else-if construction condition end not found! (Starts at: '.$start.')', E_USER_ERROR); self::error404();} else {return null;}
					}
					/* то */
					$if = self::checki(substr($in, $cond[1], $inlen), ']:[');
					if ($if === null)
					{
						/* @todo: i18n */
						if ($trigerror) {trigger_error('ERROR [TPL_SYNTAX]: Else-if construction result part end not found! (Starts at: '.$start+$cond[1].')', E_USER_ERROR); self::error404();} else {return null;}
					}
					/* иначе */
					$else = substr($in, $cond[1]+$if[1], $inlen);
					
					if (self::handle($cond[0], false))
					{
						$cresult = self::handle($if[0], false);
					} else {
						$cresult = self::handle($else, false);
					}
					
					$result = str_replace('{['.$in.']}', $cresult, $result);
					break;
				}
				$endoff = $end+2;
			}
			if (!$endfound)
			{
				/* @todo: i18n */
				if ($trigerror) {trigger_error('ERROR [TPL_SYNTAX]: Else-if construction end not found! (Starts at: '.$start.')', E_USER_ERROR); self::error404();} else {return null;}
			}
		}
		return ($startfound?$result:null);
	}
	
	public static function handle($source, $trigerror=true)
	{
		/* Пример: <-head->something in my head<-head-> */
		while (($stpss = strpos($source, '<-head->')) !== FALSE)
		{
			$t = $stpss+8;
			if (($stpse = strpos($source, '<-head->', $t)) !== FALSE)
			{
				$val = substr($source, $t, $stpse-$t);
				_RESPONSE::setHeadContent(_RESPONSE::getHeadContent().self::replacesysvars($val));
				$source = str_replace ('<-head->'.$val.'<-head->', '', $source);
			} else {
				/* @todo: i18n */
				trigger_error('ERROR [TPL_SYNTAX]: Head injection end not found! (Starts at: '.$stpss.')', E_USER_ERROR); self::error404();
			}
		}
		/* Пример: <-script->...<-script-> */
		while (($stpss = strpos($source, '<-script->')) !== FALSE)
		{
			$t = $stpss+10;
			if (($stpse = strpos($source, '<-script->', $t)) !== FALSE)
			{
				$val = substr($source, $t, $stpse-$t);
				_RESPONSE::addScript(self::replacesysvars($val));
				$source = str_replace ('<-script->'.$val.'<-script->', '', $source);
			} else {
				/* @todo: i18n */
				trigger_error('ERROR [TPL_SYNTAX]: Script injection end not found! (Starts at: '.$stpss.')', E_USER_ERROR); self::error404();
			}
		}
			
		/* Конструкция ЕСЛИ-ИНАЧЕ */
		/* Пример: {[Logged in]?[Logged in!]:[Please log in to leave comments!]} */
		$sc = self::searchcond($source, $trigerror);
		if ($sc !== null) {$source = $sc;}
		
		/* Конструкция ПОДКЛЮЧЕНИЕ ШАБЛОНА */
		/* Пример: {+footer+} */
		while (($stpss = strpos($source, '{+')) !== FALSE)
		{
			$t = $stpss+2;
			if (($stpse = strpos($source, '+}', $t)) === FALSE)
			{
				/* @todo: i18n */
				trigger_error('ERROR [TPL_SYNTAX]: Template include call end not found! (Starts at: '.$stpss.')', E_USER_ERROR); self::error404();
			}
			$val = substr($source, $t, $stpse-$t);
			$file = TPLPATH.Core::$_cfg['Theme'].DS.Core::$_cfg['ContentLanguage'].DS.$val.'.php';
			if (file_exists($file))
			{
				$tpl = file_get_contents($file);
			} else {
				trigger_error('ERROR [ROUTES]: '.Core::$_i18n['tplstr'].' "'.$val.'" '.Core::$_i18n['notfound'].'!', E_USER_WARNING); self::error404();
			}
			$source = str_replace ('{+'.$val.'+}', self::handle($tpl), $source);
		}
		
		/* Пример: {$Standart Auth->LoginPanel$} */
		/* @todo: Прилепить передачу параметров */
		while (($stpss = strpos($source, '{$')) !== FALSE)
		{
			$t = $stpss+2;
			if (($stpse = strpos($source, '$}', $t)) !== FALSE)
			{
				$val = substr($source, $t, $stpse-$t);
				$source = str_replace('{$'.$val.'$}', self::handle(self::doin($val, true), false), $source);
				Core::$_module = array();
			} else {
				/* @todo: i18n */
				trigger_error('ERROR [TPL_SYNTAX]: Component call end not found! (Starts at: '.$stpss.')', E_USER_ERROR); self::error404();
			}
		}
		
		/* Пример: {#Standart Auth->LoginButton#} */
		while (($stpss = strpos($source, '{#')) !== FALSE)
		{
			$t = $stpss+2;
			if (($stpse = strpos($source, '#}', $t)) !== FALSE)
			{
				$val = substr($source, $t, $stpse-$t);
				$source = str_replace('{#'.$val.'#}', self::doin($val, false), $source);
				Core::$_module = array();
			} else {
				/* @todo: i18n */
				trigger_error('ERROR [TPL_SYNTAX]: Phrase call end not found! (Starts at: '.$stpss.')', E_USER_ERROR); self::error404();
			}
		}
		return $source;
	}
	
	private static function recinc($lmi, $modulename, $modulepath)
	{
		foreach ($lmi as $im)
		{
			if ($im['url'] == $modulename)
			{
				if ($im['pname'] == '')
				{
					if (!self::inc_mod($modulepath))
					{
						trigger_error('ERROR [ROUTES]: '.Core::$_i18n['modulestr'].' "'.$modulename.'" '.Core::$_i18n['notfound'].'!', E_USER_WARNING); self::error404();
					}
				} else {
					foreach ($lmi as $pim)
					{
						if ($pim['name'] == $im['pname'] && !class_exists('Module_'.$pim['url'].'_controller'))
						{
							/* @todo: ПРОТЕСТИРОВАТЬ! */
							self::recinc($lmi, substr($modulepath, 0, -strlen($im['url'])), substr($modulepath, 0, -strlen($im['url'])-1).DS);
						}
					}
				}
			}
		}
	}
	
	private static function doin($matches, $comps)
	{
		/* Получение контента из модуля */
		$modulename = '';$modulepath = '';$actionname = '';
		
		$mods = explode('->', $matches);
		$c = count($mods);
		
		/* Получение установленных модулей */
		$lmi = _PDO::q(Core::$_db, "SELECT * FROM `modules`");
		if ($lmi === FALSE) {return FALSE;}
		$lmi = $lmi->fetchAll();
		
		if (!(is_array($lmi) && count($lmi) > 0)) {trigger_error('ERROR [ROUTES]: '.Core::$_i18n['notfound'].' '.Core::$_i18n['installedmods'].'!', E_USER_WARNING); self::error404();}
		
		for ($i=0;$i<$c;$i++)
		{
			if ($i == $c-1)
			{
				$compname = $mods[$i];
			} else {
				$modulename = $mods[$i];
				foreach ($lmi as $im)
				{
					if ($modulename == $im['name'])
					{
						$modulename = $im['url'];
						break;
					}
				}
				$modulepath .= $modulename.DS;
				self::recinc($lmi, $modulename, $modulepath);
			}
		}
		
		$mod = self::instantiate($modulename, !$comps, $modulepath);
		$info = $mod->info();
		Core::$_module = Core::$_module+array('object' => $mod)+$info;
		if ($info['compatible'] != Core::build_info())
		{
			trigger_error('ERROR [MODULE]: '.Core::$_i18n['modulestr'].' "'.$modulename.'" '.Core::$_i18n['notcompatible'].'!', E_USER_WARNING); self::error404();
		}
		/* Если компоненты, то берём компонент, если же фразы - то фразу */
		if ($comps)
		{
			foreach ($info['components'] as $component)
			{
				if ($component['name'] == $compname)
				{
					if (method_exists($mod, $component['link']))
					{
						return @$mod->$component['link']();
					} else {
						trigger_error('ERROR [ROUTES]: '.Core::$_i18n['methodstr'].' '.$component['link'].' '.Core::$_i18n['notfound'].' '.MODPATH.$modulepath.'!', E_USER_WARNING); self::error404();
						self::error404();
					}
				}
			}
			trigger_error('ERROR [MODULE]: '.Core::$_i18n['componentstr'].' '.$compname.' '.Core::$_i18n['notfound'].' '.MODPATH.$modulepath.'!', E_USER_WARNING); self::error404();
		} else {
			foreach ($info['phrases'] as $phrase)
			{
				if ($phrase['name'] == $compname)
				{
					if (array_key_exists($phrase['link'], _MODULE::$_i18n))
					{
						return _MODULE::$_i18n[$phrase['link']];
					} else {
						trigger_error('ERROR [ROUTES]: '.Core::$_i18n['phrasestr'].' '.$phrase['link'].' '.Core::$_i18n['notfound'].' '.MODPATH.$modulepath.'!', E_USER_WARNING); self::error404();
						self::error404();
					}
				}
			}
			trigger_error('ERROR [MODULE]: '.Core::$_i18n['phrasestr'].' '.$compname.' '.Core::$_i18n['notfound'].' '.MODPATH.$modulepath.'!', E_USER_WARNING); self::error404();
		}
	}
	
	/* TODO: 404 Error! TO 404 Error page. */
	public static function error404() {die('404 Error!');}
	
	/**
	 * Получение модуля
	 * 
	 * @param $module_path - путь к модулю
	 */
	public static function inc_mod($path)
	{
		$controller = MODPATH.$path."controller.php";
		if(file_exists($controller)) {include_once $controller;} else {return FALSE;}
		$i18n = _I18N::getcl($path);
		if ($i18n !== FALSE)
		{
			_MODULE::$_i18n = array_merge((array) _MODULE::$_i18n, (array) $i18n);
		}
		if (file_exists(MODPATH.$path."themes".DS.Core::$_cfg['Theme'].".css"))
		{
			_RESPONSE::addStyle($path);
		}
		return TRUE;
	}
	
	/**
	 * Возвращает TRUE, если экземпляр модуля был создан для получения информации о нём
	 *
	 */
	public static function isJustForInfo()
	{
		return self::$jfinfo;
	}
	
	/**
	 * Получение экземпляра модуля
	 *
	 * @param $modname - системное имя модуля
	 * @param $jfinfo - если TRUE, то isJustForInfo() будет возвращать TRUE
	 * @param $modpath - путь к модулю
	 */
	public static function instantiate($modname, $jfinfo, $modpath)
	{
		self::$jfinfo = $jfinfo;
		$mod = 'Module_'.$modname.'_controller';
		if (!$jfinfo) {Core::$_module = array('sysname' => $modname, 'syspath' => MODPATH.$modpath, 'path' => $modpath);}
		return new $mod;
	}
}