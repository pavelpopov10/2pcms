# [ZENit](http://designandcode.ru/zenit/)

ZENit is a powerful, responsive and lightweight framework built with CSS. ZENit is based on Pure. Some of the modules is the edition of the relevant modules from Pure, other modules created from scratch.

## Features

* The set of main web elements that are implemented only on CSS.
* Easy to use. Write less code, do more.
* Less size. 57 KB – ZIP-archive with a minimized version of ZENit and icons

## Quick Start

* [Download latest version](https://github.com/genteelknight/zenit/archive/master.zip)
* Copy **“css”** folder to your project. Minimally you need `zenit.min.css` file and `fonts` folder
* Create a simple HTML5 document with ZENit stylesheet
```
    <!doctype html>
    <html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <title>Page Name</title>
        
        <!-- ZENit stylesheet -->
        <link rel="stylesheet" href="css/zenit.min.css">
    
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->
    </head>
    <body>
    
    </body>
    </html>
```
* [Read the Documentation & write the code](http://designandcode.ru/zenit/category/docs/)

## What next:

* LESS version of ZENit
* New stylish UI
* Get rid of the Pure code