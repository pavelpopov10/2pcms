<? defined('COREPATH') or die('ERROR [403]: No direct script access!');
/***************************************************
*	2PCMS by Pavel Popov © 2013
*	Расположение: ./install.php
*	Описание: Установщик
***************************************************/
function test($codename)
{
	global $f;
	$text = "<tr><th>".$codename['title']."</th>";
	if ($codename['text'] == NULL) {$codename['text'] = 'Готово к работе';}
	if ($codename['pass']) {$text .= "<td class='pass'>".$codename['text']."</td></tr>";}
	else {$f = TRUE;$text .= "<td class='fail'>".$codename['text']."</td></tr>";}
	echo $text;
}
function cfg($codename)
{
	$text = "<tr><th>".$codename['text']."</th>";
	$text .= "<td class='pass'>".$codename['title']."</td></tr>";
	echo $text;
}

if (!isset($_POST['step'])) {$step = NULL;} else {$step = $_POST['step'];}
if (version_compare(PHP_VERSION, '5.3', '<')) {clearstatcache();} else {clearstatcache(TRUE);}
if (file_exists(COREPATH.'dbconf.ini') || file_exists(TPLPATH.'routes.ini') || file_exists(COREPATH.'mainconf.ini')) {
	if ($step == NULL) {exit('2PCMS уже была установленна. Удалите файл install.php!');}
	elseif ($step == 2) {unlink(COREPATH.'mainconf.ini');unlink(COREPATH.'dbconf.ini');unlink(TPLPATH.'routes.ini');}
}
echo '
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Установка 2PCMS</title>
		<style>
			body { width: 80%; margin: 0 auto; font-family: sans-serif; background: #000; font-size: 1em; color: #0f0}
			h3 + p { margin: 0 0 5px; color: #fa0; font-size: 85%; font-style: italic; }
			a { color: #fa0 }
			table { border-collapse: collapse; width: 100%; }
				table th,
				table td { padding: 0.4em; text-align: left; vertical-align: top; }
				table th { width: 12em; font-weight: normal; }
				table tr:nth-child(odd) { background: #121; }
				table td.pass { color: #0a0; }
				table td.fail { color: #911; }
			#results { padding: 0.8em; font-size: 1.5em; }
			#results.pass { 
				/* Gradient */
				background: #0a0;
				background: -moz-linear-gradient(left,  #0a0 0%, #000 100%);
				background: -webkit-gradient(linear, left top, right top, color-stop(0%,#0a0), color-stop(100%,#000));
				background: -webkit-linear-gradient(left,  #0a0 0%,#000 100%);
				background: -o-linear-gradient(left,  #0a0 0%,#000 100%);
				background: -ms-linear-gradient(left,  #0a0 0%,#000 100%);
				background: linear-gradient(to right,  #0a0 0%,#000 100%);
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#0a0", endColorstr="#000",GradientType=1 );
				color: #ff0;
				margin-bottom: -0.1%;
			}
			#results.fail { 
				background: #911;
				background: -moz-linear-gradient(left,  #911 0%, #000 100%);
				background: -webkit-gradient(linear, left top, right top, color-stop(0%,#911), color-stop(100%,#000));
				background: -webkit-linear-gradient(left,  #911 0%,#000 100%);
				background: -o-linear-gradient(left,  #911 0%,#000 100%);
				background: -ms-linear-gradient(left,  #911 0%,#000 100%);
				background: linear-gradient(to right,  #911 0%,#000 100%);
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#911", endColorstr="#000",GradientType=1 );
				color: #ff0;
				margin-bottom: -0.1%;
			}
			
			header { 
				width: 100%;
				/* Gradient */
				background: #000000;
				background: -moz-linear-gradient(top,  #000000 0%, #00aa00 55%, #006600 61%, #44ff44 64%, #00aa00 68%, #000000 100%);
				background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#000000), color-stop(55%,#00aa00), color-stop(61%,#006600), color-stop(64%,#44ff44), color-stop(68%,#00aa00), color-stop(100%,#000000));
				background: -webkit-linear-gradient(top,  #000000 0%,#00aa00 55%,#006600 61%,#44ff44 64%,#00aa00 68%,#000000 100%);
				background: -o-linear-gradient(top,  #000000 0%,#00aa00 55%,#006600 61%,#44ff44 64%,#00aa00 68%,#000000 100%);
				background: -ms-linear-gradient(top,  #000000 0%,#00aa00 55%,#006600 61%,#44ff44 64%,#00aa00 68%,#000000 100%);
				background: linear-gradient(to bottom,  #000000 0%,#00aa00 55%,#006600 61%,#44ff44 64%,#00aa00 68%,#000000 100%);
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#000000", endColorstr="#000000",GradientType=0 );
			}
			#logo { padding-left: 1%; display: inline; }
			#name { margin-left: 21%; display: inline; color: #af0; font-size: 3em; font-style: italic; vertical-align: top; line-height: 130px; text-shadow: 4px 4px 5px rgba(0, 0, 0, 0.5); }
			
			button {
				-moz-box-shadow:inset 0px -12px 20px -50px #0a0;
				-webkit-box-shadow:inset 0px -12px 20px -50px #0a0;
				box-shadow:inset 0px -12px 20px -50px #0a0;
				background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #0a0), color-stop(1, #000));
				background:-moz-linear-gradient(top, #0a0 5%, #000 100%);
				background:-webkit-linear-gradient(top, #0a0 5%, #000 100%);
				background:-o-linear-gradient(top, #0a0 5%, #000 100%);
				background:-ms-linear-gradient(top, #0a0 5%, #000 100%);
				background:linear-gradient(to bottom, #0a0 5%, #000 100%);
				filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#0a0", endColorstr="#000",GradientType=0);
				background-color:#0a0;
				display:inline-block;
				cursor:pointer;
				color:#fa0;
				font-family:Arial;
				font-size:20px;
				text-decoration:none;
				text-shadow:1px 1px 5px #000;
				width: 100%;
				border: none;
			}
			button:hover {
				background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #0f0), color-stop(1, #000));
				background:-moz-linear-gradient(top, #0f0 5%, #000 100%);
				background:-webkit-linear-gradient(top, #0f0 5%, #000 100%);
				background:-o-linear-gradient(top, #0f0 5%, #000 100%);
				background:-ms-linear-gradient(top, #0f0 5%, #000 100%);
				background:linear-gradient(to bottom, #0f0 5%, #000 100%);
				filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#0f0", endColorstr="#000",GradientType=0);
				background-color:#0f0;
			}
			button:active {
				position:relative;
				top:1px;
			}
			
			.form-field {
				border: 2px solid #333333;
				background: #000000;
				-webkit-border-radius: 0px;
				-moz-border-radius: 0px;
				border-radius: 0px;
				color: #00ff00;
				-webkit-box-shadow: rgba(255,255,255,0.4) 0 1px 0, inset rgba(000,000,000,0.7) 0 1px 1px;
				-moz-box-shadow: rgba(255,255,255,0.4) 0 1px 0, inset rgba(000,000,000,0.7) 0 1px 1px;
				box-shadow: rgba(255,255,255,0.4) 0 1px 0, inset rgba(000,000,000,0.7) 0 1px 1px;
				width:100%;
			}
			.form-field:focus {
				background: #003300;
				color: #00ff00;
			}
		</style>
	</head>
	<body>
		<header>
			<div id="logo"><img src="2p_logo.png" width="150px" height="150px"></div>
			<div id="name">Установщик 2PCMS</div>
		</header>
';

switch ($step)
{
	case 1:
	
		echo '
			<h3>Основные настройки</h3>
			<p>Перед использованием движка, необходимо настроить основные параметры движка.</p>
			<form action="/" method="POST">
			<table cellspacing="0">
		';
	
		$main_domname = array ('title' => 'Введите доменное имя сайта без указания протокола, www и / в конце (Пример: site.ru)');
		$main_domname['text'] = '<input class="form-field" type="text" name="main_domname" value="" maxlength="255" />';
	
		$select_errors = array ('title' => 'Выберите вариант обработки ошибок');
		$select_errors['text'] = '
		<select class="form-field" name="main_errors">
			<option value="work_log">Помещать в лог</option>
			<option value="work_show_log">Показывать и помещать в лог</option>
			<option value="work_log_trace">Помещать в лог (с трассировкой)</option>
			<option value="work_show_trace">Показывать (с трассировкой)</option>
			<option value="work_show_log_trace">Показывать и помещать в лог (с трассировкой)</option>
			<option value="nowork">Не обрабатывать</option>
		</select>';

		$select_logging = array ('title' => 'Выберите вариант обработки логов');
		$select_logging['text'] = '
		<select class="form-field" name="main_logging">
			<option value="work_files">Помещать записи в лог-файлы</option>
			<option value="nowork">Не помещать никаких записей в лог</option>
		</select>';

		$input_main_logging_days = array ('title' => 'Введите кол-во дней хранения записей в логе');
		$input_main_logging_days['text'] = '<input class="form-field" type="text" name="main_logging_days" value="7" maxlength="3" />';

		$select_compression = array ('title' => 'Выберите вариант сжатия страниц');
		$select_compression['text'] = '
		<select class="form-field" name="main_compression">
			<option value="work">Сжимать</option>
			<option value="nowork">Не сжимать</option>
		</select>';

		cfg($main_domname);
		cfg($select_errors);
		cfg($select_logging);
		cfg($input_main_logging_days);
		cfg($select_compression);
	
		echo '
			</table>
			<h3>Настройки СУБД</h3>
			<p>Кроме основных параметров, требуется настроить соединение с базой данных.</p>
			<table cellspacing="0">
		';
		$db_ext = array();
		if (class_exists('PDO'))
		{
			if(in_array('mysql', PDO::getAvailableDrivers())) $db_ext[] = array('mysql', 'MySQL InnoDB');
			/* НЕ РАБОТАЕТ!
			if(in_array('sqlite', PDO::getAvailableDrivers())) $db_ext[] = array('pdo_sqlite', 'SQLite');
			if(in_array('dblib', PDO::getAvailableDrivers())) $db_ext[] = array('pdo_dblib', 'FreeTDS / MS SQL Server / Sybase');
			if(in_array('firebird', PDO::getAvailableDrivers())) $db_ext[] = array('pdo_firebird', 'Firebird / Interbase 6');
			if(in_array('ibm', PDO::getAvailableDrivers())) $db_ext[] = array('pdo_ibm', 'IBM DB2');
			if(in_array('informix', PDO::getAvailableDrivers())) $db_ext[] = array('pdo_informix', 'IBM Informix Dynamic Server');
			if(in_array('oci', PDO::getAvailableDrivers())) $db_ext[] = array('pdo_oci', 'Oracle Call Interface');
			if(in_array('odbc', PDO::getAvailableDrivers())) $db_ext[] = array('pdo_odbc', 'ODBC v3 (IBM DB2, unixODBC, win32 ODBC)');
			if(in_array('pgsql', PDO::getAvailableDrivers())) $db_ext[] = array('pdo_pgsql', 'PostgreSQL');
			if(in_array('sqlsrv', PDO::getAvailableDrivers())) $db_ext[] = array('pdo_sqlsrv', 'MS SQL Server / SQL Azure');
			if(in_array('4D', PDO::getAvailableDrivers())) $db_ext[] = array('pdo_4D', '4D');
			*/
		}

		$select_db = array ('title' => 'Выберите Вашу СУБД в представленном списке');
		$select_db['text'] = '<select class="form-field" name="dbms_driver">';
		foreach ($db_ext as $db_type)
		{
			$select_db['text'] .= '<option value="'.$db_type[0].'">'.$db_type[1].'</option>';
		}
		$select_db['text'] .= '</select></form>';

		$input_db_host = array ('title' => 'Введите адрес сервера, на котором установленна СУБД');
		$input_db_host['text'] = '<input class="form-field" type="text" name="dbms_host" value="localhost" maxlength="75" />';

		$input_db_port = array ('title' => 'Введите порт сервера, на котором установленна СУБД');
		$input_db_port['text'] = '<input class="form-field" type="text" name="dbms_port" value="3306" maxlength="6" />';

		$input_db_dbname = array ('title' => 'Введите название базы данных');
		$input_db_dbname['text'] = '<input class="form-field" type="text" name="dbms_dbname" maxlength="50" />';

		$input_db_usrname = array ('title' => 'Введите имя пользователя для подключения к базе данных');
		$input_db_usrname['text'] = '<input class="form-field" type="text" name="dbms_usrname" maxlength="50" />';

		$input_db_passw = array ('title' => 'Введите пароль пользователя для подключения к базе данных');
		$input_db_passw['text'] = '<input class="form-field" type="password" name="dbms_passw" maxlength="75" />';

		$input_db_charset = array ('title' => 'Введите кодировку соединения с базой данных');
		$input_db_charset['text'] = '<input class="form-field" type="text" value="UTF8" name="dbms_charset" maxlength="25" />';

		
		cfg($select_db);
		cfg($input_db_host);
		cfg($input_db_port);
		cfg($input_db_dbname);
		cfg($input_db_usrname);
		cfg($input_db_passw);
		cfg($input_db_charset);
		
		echo '
			</table>
			<h3>Настройки аккаунта администратора</h3>
			<p>Последнее - настроить аккаунт администратора.</p>
			<table cellspacing="0">
		';

		$input_admin_login = array ('title' => 'Введите логин администратора');
		$input_admin_login['text'] = '<input class="form-field" type="text" name="admin_login" maxlength="32" value="admin" />';

		$input_admin_pass = array ('title' => 'Введите пароль администратора');
		$input_admin_pass['text'] = '<input class="form-field" type="password" name="admin_pass" maxlength="32" value="admin" />';
		
		cfg($input_admin_login);
		cfg($input_admin_pass);
		
		echo "</table><button type='submit'><h3>Дальше</h3></button><input type='hidden' name='step' value='2'></form>";
		break;
	case 2:
		echo '
			<h3>Установка</h3>
			<p>Установка и настройка движка по заданным параметрам...</p>
			<table cellspacing="0">
		';
		$f = FALSE;
		$cfg = array();
		foreach($_POST as $key => $value) {$cfg[$key] = htmlspecialchars(trim($value));}

		$error_handling = array();
		$logging = array();
		$page_compression = null;
		$admin = array('login' => $cfg['admin_login'], 'pass' => $cfg['admin_pass']);
		switch($cfg['main_errors'])
		{
			case 'work_log':$error_handling['work'] = 'on';$error_handling['logging'] = 'on';$error_handling['show'] = 'off';$error_handling['tracing'] = 'off';break;
			case 'work_show_log':$error_handling['work'] = 'on';$error_handling['logging'] = 'on';$error_handling['show'] = 'on';$error_handling['tracing'] = 'off';break;
			case 'work_log_trace':$error_handling['work'] = 'on';$error_handling['logging'] = 'on';$error_handling['show'] = 'off';$error_handling['tracing'] = 'on';break;
			case 'work_show_trace':$error_handling['work'] = 'on';$error_handling['logging'] = 'off';$error_handling['show'] = 'on';$error_handling['tracing'] = 'on';break;
			case 'work_show_log_trace':$error_handling['work'] = 'on';$error_handling['logging'] = 'on';$error_handling['show'] = 'on';$error_handling['tracing'] = 'on';break;
			default:$error_handling['work'] = 'off';$error_handling['logging'] = 'off';$error_handling['show'] = 'off';$error_handling['tracing'] = 'off';break;
		}
		switch($cfg['main_logging'])
		{
			case 'work_files':$logging['work'] = 'on';$logging['place'] = 'files';break;
			default:$logging['work'] = 'off';$logging['place'] = 'files';break;
		}
		$logging['days'] = intval($cfg['main_logging_days']);
		switch($cfg['main_compression'])
		{
			case 'work':$page_compression = 'on';break;
			default:$page_compression = 'off';break;
		}

		try {
			/* Ini конфигурация подключения к субд */
			if (!_INI::write(array('driver' => $cfg['dbms_driver'], 'host' => $cfg['dbms_host'], 'dbname' => $cfg['dbms_dbname'], 'port' => $cfg['dbms_port'], 'charset' => $cfg['dbms_charset'], 'usrname' => $cfg['dbms_usrname'], 'passw' => $cfg['dbms_passw']), COREPATH.'dbconf.ini', false, 'Данные для подключения к СУБД')) {$f = TRUE;}
			/* Ini страницы */
			if ($f == TRUE || !_INI::write(array(
					'news\/edit'	=> array('page' => 'editpost', 	'title' => 'Редактирование новости','mimetype_lock' => 'on', 	'title_lock' => 'on'),
					'news\/add' 	=> array('page' => 'addpost', 	'title' => 'Добавление новости', 	'mimetype_lock' => 'on', 	'title_lock' => 'on'),
					'admin' 		=> array('page' => 'admin', 	'title' => 'Панель управления', 	'mimetype_lock' => 'on', 	'title_lock' => 'off'),
					'contacts' 		=> array('page' => 'contacts', 	'title' => 'Контакты', 				'mimetype_lock' => 'on', 	'title_lock' => 'on'),
					'about' 		=> array('page' => 'about', 	'title' => 'О нас', 				'mimetype_lock' => 'on', 	'title_lock' => 'on'),
					'b' 			=> array('page' => 'blog', 		'title' => 'Блог', 					'mimetype_lock' => 'on', 	'title_lock' => 'on'),
					'services'		=> array('page' => 'services', 	'title' => 'Услуги', 				'mimetype_lock' => 'on', 	'title_lock' => 'on'),
					'index' 		=> array('page' => 'main', 		'title' => 'Главная', 				'mimetype_lock' => 'on', 	'title_lock' => 'off'),
					'' 				=> array('page' => '404', 		'title' => 'Упс... Ошибочка вышла!','mimetype_lock' => 'on', 	'title_lock' => 'on')
			), TPLPATH.'routes.ini', true, 'Страницы сайта')) {$f = TRUE;}
			/* Ini конфигурация движка */
			if (!_INI::write(array(
					'DomainName' => $cfg['main_domname'],
					'ErrorHandling' => $error_handling['work'],
					'ErrorHandling_logging' => $error_handling['logging'],
					'ErrorHandling_show' => $error_handling['show'],
					'ErrorHandling_tracing' => $error_handling['tracing'],
					'Logging' => $logging['work'],
					'Logging_days' => $logging['days'],
					'Logging_place' => $logging['place'],
					'PageCompression' => $page_compression,
					'Theme' => 'classic',
					'Language' => 'ru'
			), COREPATH.'mainconf.ini', false, 'Настройки системы')) {$f = TRUE;}
			if ($f == TRUE || !$db = _PDO::c()) {$f = TRUE;}
			/* Только под MySQL InnoDB
			 Проверка на доступность InnoDB */
			if ($f == TRUE || $cfg['dbms_driver'] == 'mysql')
			{
				$sth = _PDO::q($db, 'SHOW VARIABLES LIKE \'innodb_version\'');
				$r = $sth->fetch();
				if ($r[1] == NULL) {echo('InnoDB не была найдена! Выберите другую СУБД или включите InnoDB.');$f = TRUE;}
				else {
					/* Создание таблицы модулей */
					if($f == TRUE || _PDO::t ($db, "
						CREATE TABLE IF NOT EXISTS `modules` (
							`name` varchar(32) NOT NULL,
							`pname` varchar(32),
							`url` varchar(32) NOT NULL,
							PRIMARY KEY (`name`)
						) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					") === FALSE) {$f = TRUE;}
					
					/* Установка модулей */
					if ($f == TRUE || !_MODMAN::install($db, "auth") || !_MODMAN::install($db, "admin") || !_MODMAN::install($db, "news"))
					{
						_MODMAN::uninstall($db, "auth");
						_MODMAN::uninstall($db, "admin");
						_MODMAN::uninstall($db, "news");
						$f = TRUE;
					}
					
					/* Добавление админа */
					if($f == TRUE || _PDO::t ($db, "
						INSERT INTO `users` (`id`, `login`, `pass`) VALUES
						(0, :login, :pass);
					", array(
						'login' => $admin['login'],
						'pass' => _CRYPT::blowfish($admin['pass'])
					)) === FALSE) {$f = TRUE;}
					
					/* Добавление новости */
					if($f == TRUE || _PDO::t ($db, "
						INSERT INTO `news` (`id`, `title`, `short`, `full`, `author`, `dt`) VALUES
						(0, 'Тестовая новость', 'Эта новость была создана при установке 2PCMS.', 'Новость нуждается в удалении!', 0, NOW());
					") === FALSE) {$f = TRUE;}
				}
			}
		} catch (Exception $e) {$f = TRUE; echo 'Произошла ошибка при установке 2PCMS. Проверьте правильность введённых Вами данных.'; _ERR::exc($e);}
		if ($f === TRUE)
		{
			unlink(COREPATH.'mainconf.ini');
			unlink(COREPATH.'dbconf.ini');
			unlink(TPLPATH.'routes.ini');
			echo '</table><p id="results" class="fail">✘ 2PCMS была установлена некорректно</p>';
		} else {
			echo '</table><p id="results" class="pass">✔ 2PCMS успешно установилась</p>';
			echo "<form action='/' method='POST'><button type='submit'><h3>Продолжить?</h3></button><input type='hidden' name='step' value='3'></form>";
		}
		break;
	case 3:
		echo "
			<h3>Спасибо за установку продукта!</h3>
			<p>2PCMS успешно установлена.</p>
			<hr>
			<p>Прежде чем начать работу с движком, необходимо удалить или переименовать файл установщика 2PCMS (install.php), находящийся в корневой директории движка.</p>
			<p>После этого Вы можете приступить к подключению модулей и оформлению сайта. Сделать это можно через Панель Управления (ПУ).</p>
			<button><a href='/'><h3>Главная</h3></a></button>
		";
		break;
	default:
		echo '
			<h3>Тесты среды</h3>
			<p>Тесты среды определят будет ли 2PCMS работать на данном сервере.</p>
			<table cellspacing="0">
		';
		$f = FALSE;

		$env_php = array ('title' => 'Версия PHP');
		$env_php_ver = '5.5.4';
		if (version_compare(PHP_VERSION, $env_php_ver, '>=')) {$env_php['pass'] = TRUE;$env_php['text'] =  PHP_VERSION;}
		else {$env_php['pass'] = FALSE;$env_php['text'] = '2PCMS требует версию PHP не ниже '.$env_php_ver.'. Ваша версия PHP: '.PHP_VERSION;}

		$env_core = array ('title' => 'Директория ядра');
		if (is_dir(COREPATH) AND is_file(COREPATH.'bootstrap.php') AND is_file(COREPATH.'core.php')) {$env_core['pass'] = TRUE;$env_core['text'] =  COREPATH;}
		else {$env_core['pass'] = FALSE;$env_core['text'] = "Установленная директория ядра не существует или не содержит необходимых файлов";}

		$env_logs = array ('title' => 'Директория логов');
		if (is_dir(LOGPATH) AND is_writable(LOGPATH)) {$env_logs['pass'] = TRUE;$env_logs['text'] =  LOGPATH;}
		else {$env_logs['pass'] = FALSE;$env_logs['text'] = "Установленная директория логов не существует или не доступна для записи";}

		$env_uri = array ('title' => 'Определение URI');
		if (isset($_SERVER['REQUEST_URI']) OR isset($_SERVER['PHP_SELF']) OR isset($_SERVER['PATH_INFO'])) {$env_uri['pass'] = TRUE;$env_uri['text'] = 'Текущий URI определён';}
		else {$env_uri['pass'] = FALSE;$env_uri['text'] = 'Не удалось определить текущий URI.';}

		$env_PDO = array ('title' => '<a href="http://www.php.net/pdo">PDO</a>');
		if (class_exists('PDO')) {$env_PDO['pass'] = TRUE;$env_PDO['text'] = NULL;}
		else {$env_PDO['pass'] = FALSE;$env_PDO['text'] = 'PDO не загружен или не входит в сборку среды';}

		$env_spl = array ('title' => '<a href="http://www.php.net/spl">SPL</a>');
		if (function_exists('spl_autoload_register')) {$env_spl['pass'] = TRUE;$env_spl['text'] = NULL;}
		else {$env_spl['pass'] = FALSE;$env_spl['text'] = 'SPL не загружен или не входит в сборку среды';}

		$env_pcre = array ('title' => '<a href="http://www.php.net/pcre">PCRE</a>');
		if (!@preg_match('/^.$/u', 'ñ')) {$env_pcre['pass'] = FALSE;$env_pcre['text'] = 'PCRE были скомпилированы без поддержки UTF-8';}
		elseif (!@preg_match('/^\pL$/u', 'ñ')) {$env_pcre['pass'] = FALSE;$env_pcre['text'] =  'PCRE были скомпилированы без поддержки Unicode';}
		else {$env_pcre['pass'] = TRUE;$env_pcre['text'] = NULL;}

		$env_iconv = array ('title' => '<a href="http://www.php.net/iconv">Iconv</a>');
		if (extension_loaded('iconv')) {$env_iconv['pass'] = TRUE;$env_iconv['text'] = NULL;}
		else {$env_iconv['pass'] = FALSE;$env_iconv['text'] = 'Iconv не загружен';}
		
		$env_blowfish = array ('title' => 'BlowFish');
		if (CRYPT_BLOWFISH == 1) {$env_blowfish['pass'] = TRUE;$env_blowfish['text'] = NULL;}
		else {$env_blowfish['pass'] = FALSE;$env_blowfish['text'] = 'BlowFish хеширование не поддерживается Вашей системой.';}

		test($env_php);test($env_core);test($env_logs);test($env_uri);
		test($env_PDO);test($env_spl);test($env_pcre);test($env_iconv);test($env_blowfish);
		$g = $f;
		echo '
			</table>
			<h3>Дополнительные тесты</h3>
			<p>Следующие расширения не требуются для корректной работы 2PCMS, однако их результат может повлиять на возможность подключать некоторые сторонние модули.</p>
			<table cellspacing="0">
		';
		$f = FALSE;

		// $opt_mcrypt = array ('title' => '<a href="http://www.php.net/mcrypt">Mcrypt</a>');
		// if (extension_loaded('mcrypt')) {$opt_mcrypt['pass'] = TRUE;$opt_mcrypt['text'] = NULL;}
		// else {$opt_mcrypt['pass'] = FALSE;$opt_mcrypt['text'] = '2PCMS требует Mcrypt для шифрования данных';}

		$opt_curl = array ('title' => '<a href="http://www.php.net/curl">cURL</a>');
		if (extension_loaded('curl')) {$opt_curl['pass'] = TRUE;$opt_curl['text'] = NULL;}
		else {$opt_curl['pass'] = FALSE;$opt_curl['text'] = '2PCMS требует cURL для работы с удалёнными серверами';}

		// test($opt_mcrypt);
		test($opt_curl);
		
		if ($g === TRUE)
		{
			echo '</table><p id="results" class="fail">✘ Возможна некорректная работа на данном сервере</p>';
		} elseif ($f === TRUE) {
			echo '</table><p id="results" class="fail">✘ Тесты среды пройдены успешно, однако некоторые модули могут вести себя некорректно</p>';
		} else {
			echo '</table><p id="results" class="pass">✔ Все тесты пройдены успешно</p>';
		}
		echo "<form action='/' method='POST'><button type='submit'><h3>Продолжить?</h3></button><input type='hidden' name='step' value='1'></form>";
}
echo '
		<footer><center><a href="http://2programm.ru">2programm</a> [Pavel Popov] © 2013</footer>
	</body>
</html>
';
Core::shutdown();
?>