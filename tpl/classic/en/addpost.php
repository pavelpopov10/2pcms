{[{$Standart Auth->IsAuthorised$}]?[
	{[{$Standart News->ActionCompleted$}]?[{-//-}]:[
		{+block/headertomain+}
		<div class="container content-pane">
			<div class="row">
				<div class="col-75">
					<form class="stacked" action="{PAGE}?act=addnewspost" method="post">
						<fieldset>
							<legend>Adding a post</legend>
							<label for="title">Headline</label>
							<input class="col-30" name="title" type="text" placeholder="Headline"><br><br>
							<label for="short">Short post</label>
							<textarea class="col-100" name="short" rows="10">Short post</textarea><br><br>
							<label for="full">Full post</label>
							<textarea class="col-100" name="full" rows="10">Full post</textarea>
							<button type="submit" class="button green" style="margin-top: 10px">Add</button>
						</fieldset>
					</form>
				</div>
				{+block/sidebar+}
			</div>
		</div>
		{+block/footer+}
	]}
]:[{-/404/-}]}