{+block/header+}
<div class="container content-pane">
	<div class="row">
		<div class="col-75">
			<form class="stacked" action="{PAGE}" method="post">
				<fieldset>
					<legend>Contact us</legend>
					<label for="email">Your E-Mail</label>
					<input class="col-30" id="email" type="email" placeholder="E-Mail"><br><br>
					<label for="theme">Message topic</label>
					<input class="col-50" id="theme" type="text" placeholder="Topic"><br><br>
					<label for="msg">Your message</label>
					<textarea class="col-100" id="msg" rows="10">Message</textarea>
					<button type="submit" class="button green" style="margin-top: 10px">Send</button>
				</fieldset>
			</form>
		</div>
		{+block/sidebar+}
	</div>
</div>
{+block/footer+}