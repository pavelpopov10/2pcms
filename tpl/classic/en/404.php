<-head->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="inc/zenit/css/zenit.min.css">
<-head->
<div class="container content-pane">
	<h1>Oops... Error occured!</h1><hr>
	<p>This page doesn't exist. Maybe the link you followed is broken.</p>
	<p>Anyway you can visit our <a href="/">main page</a>.</p>
</div>
{+block/footer+}