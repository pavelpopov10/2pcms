{+block/headscripts+}
<nav class="menu">
	<ul class="clearfix">
		<li class="visible-phone">
			<i class="icon-reorder"></i> Show Menu
		</li>
		<li class="menu-heading"><a href="/" class="icon-home">2PCMS</a></li>
		<li class="menu-parent">
			<a href="/services">Services</a>
			<ul>
				<li><a href="/services">Offer 1</a></li>
				<li class="menu-parent">
					<a href="/services">Offer 2</a>
					<ul>
						<li><a href="/services">Offer 2.1</a></li>
					</ul>
				</li>
				<li><a href="/services">Offer 3</a></li>
			</ul>
		</li>
		<li><a href="/b">Blog</a></li>
		<li><a href="/about">About</a></li>
		<li><a href="/contacts">Contacts</a></li>
		<li class="alignright">
			{[{$Standart Auth->IsAuthorised$}]?[
				<a href="{PAGE}">{$Standart Auth->GetLogin$}</a>
				<ul>
					<li><a href="/admin">Control panel</a></li>
					<li><a href="?act=logout">Logout</a></li>
				</ul>
			]:[{+block/loginform+}]}
		</li>
	</ul>
</nav>
{+block/progressbar+}