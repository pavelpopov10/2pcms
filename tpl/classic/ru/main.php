{+block/header+}
<div class="container content-pane">
	<div class="row">
		<div class="col-75">
			<legend>Новости</legend>
			{[{$Standart Auth->IsAuthorised$}]?[<a href="/news/add" class="icon-plus-sign icon right top green"></a>]:[]}<hr>
			{$Standart News->GetNews$}
		</div>
		{+block/sidebar+}
	</div>
</div>
{+block/footer+}