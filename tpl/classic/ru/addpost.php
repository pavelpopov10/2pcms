{[{$Standart Auth->IsAuthorised$}]?[
	{[{$Standart News->ActionCompleted$}]?[{-//-}]:[
		{+block/headertomain+}
		<div class="container content-pane">
			<div class="row">
				<div class="col-75">
					<form class="stacked" action="{PAGE}?act=addnewspost" method="post">
						<fieldset>
							<legend>Добавление новости</legend>
							<label for="title">Заголовок</label>
							<input class="col-30" name="title" type="text" placeholder="Заголовок"><br><br>
							<label for="short">Короткая новость</label>
							<textarea class="col-100" name="short" rows="10">Короткая новость</textarea><br><br>
							<label for="full">Полная новость</label>
							<textarea class="col-100" name="full" rows="10">Полная новость</textarea>
							<button type="submit" class="button green" style="margin-top: 10px">Добавить</button>
						</fieldset>
					</form>
				</div>
				{+block/sidebar+}
			</div>
		</div>
		{+block/footer+}
	]}
]:[{-/404/-}]}