{+block/header+}
<div class="container content-pane">
	<div class="row">
		<div class="col-75">
			<form class="stacked" action="{PAGE}" method="post">
				<fieldset>
					<legend>Свяжитесь с нами</legend>
					<label for="email">Ваш E-Mail</label>
					<input class="col-30" id="email" type="email" placeholder="E-Mail"><br><br>
					<label for="theme">Тема сообщения</label>
					<input class="col-50" id="theme" type="text" placeholder="Тема"><br><br>
					<label for="msg">Ваше сообщение</label>
					<textarea class="col-100" id="msg" rows="10">Сообщение</textarea>
					<button type="submit" class="button green" style="margin-top: 10px">Отправить</button>
				</fieldset>
			</form>
		</div>
		{+block/sidebar+}
	</div>
</div>
{+block/footer+}