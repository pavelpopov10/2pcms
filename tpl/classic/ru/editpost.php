{[{$Standart Auth->IsAuthorised$}]?[
	{[{$Standart News->ActionCompleted$}]?[{-//-}]:[
		{[{$Standart News->IsIdSet$}]?[
			{+block/headertomain+}
			<div class="container content-pane">
				<div class="row">
					<div class="col-75">
						<form class="stacked" action="{PAGE}?act=editnewspost&id={$Standart News->GetId$}" method="post">
							<fieldset>
								<legend>Редактирование новости</legend>
								<label for="title">Заголовок</label>
								<input class="col-30" name="title" type="text" placeholder="Заголовок" value="{$Standart News->GetPostTitle$}"><br><br>
								<label for="short">Короткая новость</label>
								<textarea class="col-100" name="short" rows="10">{$Standart News->GetPostShort$}</textarea><br><br>
								<label for="full">Полная новость</label>
								<textarea class="col-100" name="full" rows="10">{$Standart News->GetPostFull$}</textarea>
								<button type="submit" class="button green" style="margin-top: 10px">Сохранить</button>
							</fieldset>
						</form>
					</div>
					{+block/sidebar+}
				</div>
			</div>
			{+block/footer+}
		]:[{-//-}]}
	]}
]:[{-/404/-}]}