<-head->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="http://{DOMAIN}/inc/zenit/css/zenit.min.css">
<-head->
<-script->
	var w = 0;
	function domload() {
		var pb = document.getElementById("menuprogressbar");
		w = w + 1;
		pb.style.width = w + '%';
		setInterval(function () {
		    if (w < 100) {
		        w = w + 1;
		        pb.style.width = w + '%';
		    }
		}, 5);
	}
	document.addEventListener("DOMContentLoaded", domload, false);
<-script->