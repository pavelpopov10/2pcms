<div class="container footer">
	<a target="_blank" href="http://2programm.ru/">2programm</a> © 2014 | Powered by <a target="_blank" href="https://bitbucket.org/pavelpopov10/2pcms/">2PCMS</a> | Thanks to <a target="_blank" href="http://designandcode.ru/zenit/">ZENit</a>
	<ul class="alignright social">
		<li><a href="https://twitter.com/" target="_blank" class="icon-twitter-sign"></a></li>
		<li><a href="http://vk.com/" target="_blank" class="icon-vk"></a></li>
		<li><a href="http://www.youtube.com/" target="_blank" class="icon-youtube-sign"></a></li>
	</ul>
</div>