{+block/headscripts+}
<nav class="menu">
	<ul class="clearfix">
		<li class="visible-phone">
			<i class="icon-reorder"></i> Show Menu
		</li>
		<li class="menu-heading"><a href="/" class="icon-home">2PCMS</a></li>
		<li class="menu-parent">
			<a href="/services">Услуги</a>
			<ul>
				<li><a href="/services">Услуга 1</a></li>
				<li class="menu-parent">
					<a href="/services">Услуга 2</a>
					<ul>
						<li><a href="/services">Услуга 2.1</a></li>
					</ul>
				</li>
				<li><a href="/services">Услуга 3</a></li>
			</ul>
		</li>
		<li><a href="/b">Блог</a></li>
		<li><a href="/about">О нас</a></li>
		<li><a href="/contacts">Контакты</a></li>
		<li class="alignright">
			{[{$Standart Auth->IsAuthorised$}]?[
				<a href="{PAGE}">{$Standart Auth->GetLogin$}</a>
				<ul>
					<li><a href="/admin">Панель управления</a></li>
					<li><a href="/?act=logout">Выйти</a></li>
				</ul>
			]:[{+block/loginform+}]}
		</li>
	</ul>
</nav>
{+block/progressbar+}