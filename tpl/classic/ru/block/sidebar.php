<aside class="sidebar bdiv">
	<center><h3 style="color: #1CB841">Боковая панель</h3></center>
	<b>Боковая панель</b> (или sidebar) - идеальное место для размещения такой информации, как ваши контакты, ссылки на социальные сети, важные новости и так далее. Однако не стоит забывать, что загруженный бессмысленными виджетами сайдбар лишь ухудшает восприятие информации и отталкивает посетителей. Прежде чем размещать на каждой странице сайта виджет часов, подумайте, нужно ли это делать? Ведь время все равно показано на панели десктопа компьютера.
</aside>